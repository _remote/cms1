<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class SiteController extends Controller {
    
    public $_model;
    
    public $_title = "";
    public $_keywords = "";
    public $_content = "";
    public $_file = "";
    public $_id_language = false;
    
    
    public $_menu = array();
    public $_path = false;
    public $_languages = false;
    
    public $_search = "";
    
    
    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
    }
    
    public function init() {
        parent::init();
        $this->_resources = Resource::model()->getResourcesList(Yii::app()->language);
    }
    
    
    public function loadModel() {
        if($this->_model===null) {
            if($this->_path) {
                $criteria = new CDbCriteria();
                $criteria->condition = "article.path = :path AND language.`type` = :language AND is_show = 1";
                $criteria->params = array(':path' => $this->_path, ':language' => Yii::app()->language);
                $this->_model=Page::model()->with(array("article" => array('joinType'=>'LEFT JOIN'), "language" => array('joinType'=>'LEFT JOIN')))->find($criteria);
                
                //Загружаем доступные языки для этой страницы
                if (!empty($this->_model['id_article'])) {
                    $criteria = new CDbCriteria();
                    $criteria->with = array("language" => array("select" => array("name", "type", "icon")));
                    $criteria->select = 't.`id`';
                    $criteria->condition = "id_article = :id_article AND is_show = 1";
                    $criteria->params = array(':id_article' => $this->_model['id_article']);
                    $this->_languages=Page::model()->findAll($criteria);
                }
                
                if (!empty($this->_model->id_language)) {
                    $this->_id_language = $this->_model->id_language;
                }
            }
            if($this->_model===null)
                throw new CHttpException(404,'The requested page does not exist.');
        }
        return $this->_model;
    }
    
    protected function isUniqueMethod() {
        if (!empty($this->_path) && strtolower($this->_path) != "index") {
            $method = "_".ucfirst($this->_path);
            if (method_exists($this, $method)) {
                $this->$method();
                Yii::app()->end();
            }
        }
    }
    
    /**
     * Index action is the default action in a controller.
     */
    public function actionIndex() {
        if(isset($_GET['path'])) 
            $this->_path = $this->p->purify($_GET['path']);
        $this->isUniqueMethod();
        $model = $this->loadModel();
        $this->_title = $model->title;
        $this->_keywords = $model->keywords;
        $this->_content = $model->content;
        $this->_file = $model->doc_file;
        $this->render("index");
    }
    
    public function actionError() {
        if ($error=Yii::app()->errorHandler->error) {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
    
    public function _Search() {
        $this->_search = htmlspecialchars($this->p->purify(@$_POST['search']));
        $results = false;
        $trim = trim($this->_search);
        if (!empty($trim) && !in_array($this->_search, array(".", "'", "\"", "=", "&"))) {
            $criteria=new CDbCriteria;
            $criteria->with = "article";
            $criteria->addSearchCondition('content', $this->_search);
            $criteria->order = "t.create_date DESC";
            $criteria->limit = 7;
            $results = Page::model()->findAll($criteria);
        }
        $this->render("search", array("results" => $results, "search" => $this->_search));
    }
    
    public function _Partners() {
        $сlist = array();
        $plist = array();
        $active = (isset($_GET['category']))?intval($_GET['category']):false;
        $_c = Categories::model()->findAll(array('order' => 'id'));
        if ($_c) {
            $сlist = CHtml::listData($_c, 'id', 'description');
            $first = key($сlist);
            $category = (isset($_GET['category']))?intval($_GET['category']):(($сlist)?$first:false);
            $_p = Partners::model()->findAll("category = :category", array(":category" => $category));
            if ($_p) {
                foreach ($_p as $p)
                    $plist[] = $p;
            }
            //$plist = CHtml::listData($_p, 'partner_link', 'description');
        }
        $this->render("partners", array("categories" => $сlist, "partners" => $plist, "active" => $active));
    }
    
    public function _Video() {
        $criteria=new CDbCriteria;

        $criteria->order = 'create_date DESC';

        $dataProvider = new CActiveDataProvider("Video", array(
                'criteria'=>$criteria,
                'pagination' => array(
                    'pageSize' => 6,
                    'route' => "/video",
                ),
        ));
        $video = new Video();
        $this->render("video", array("dataProvider" => $dataProvider));
    }
    
    public function actionComment() {
        $is_approved = false;
        $model=new Comment;

        // uncomment the following code to enable ajax-based validation
        if(isset($_POST['ajax']) && $_POST['ajax']==='comment-comment-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        
        if(isset($_POST['Comment'])) {
            $model->attributes=$_POST['Comment'];
            $language = Language::model()->find("`type` = :type", array(":type" => Yii::app()->language));
            $model->id_language = ($language)?$language->id:0;
            $model->create_date = $this->_now;
            $model->is_approved = 0;
            $model->name = htmlspecialchars($model->name);
            $model->text = htmlspecialchars($model->text);
            $model->scenario = 'registerwcaptcha';

            if($model->validate()) {
                $model->scenario = NULL;
                if ($model->save()) {
                    $model->name = $model->email = $model->text = "";
                    $is_approved = true;
                }
                
            }
        }
        
        
        $comments = Comment::model()->with("language")->findAll("language.`type` = :type AND is_approved = 1", array(":type" => Yii::app()->language));
        $this->render("comment", array("model" => $model, "comments" => $comments, "is_approved" => $is_approved));
    }
}