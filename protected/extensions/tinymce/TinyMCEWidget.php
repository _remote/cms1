<?php
class TinyMCEWidget extends CInputWidget {

    public $options;

    public function run() {
        $this->options['editor'] = "full";
        list($name, $id) = $this->resolveNameID();

        // Publishing assets.
        $assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__));

        // Registering javascript.
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');

        $cs->registerScriptFile($assets . '/tinymce.min.js');
        // Подключаем файл настроек редактора, если он указан и существует
        $time = time();
        if(isset($this->options['editor'])) {
            if (file_exists(dirname(Yii::app()->basePath).$assets.'/config.'.$this->options['editor'].'.js')) {
                $cs->registerScriptFile($assets.'/config.'.$this->options['editor'].'.js'.'?t='.$time);
            }
        }

        $cs->registerScript(
            'Yii.' . get_class($this) . '#' . $id, '$(function(){$("#' . $id . '").tinymce(' . CJavaScript::encode($this->options) . ');});'
        );

        // подключаем css стили если они указаны в опциях * пока жёстко *
        $css = '';
        $css .= (isset($this->options['height']) and $this->options['height']) ? 'height:'.$this->options['height'].';' : '';
        $css .= (isset($this->options['width']) and $this->options['width']) ? 'width:'.$this->options['width'].';' : '';
        if ($css !== '') $this->htmlOptions['style'] = $css;
        $this->htmlOptions['class'] = "_TM_area";

        if ($this->hasModel()) {
            $html = CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
        } else {
            $html = CHtml::textArea($name, $this->value, $this->htmlOptions);
        }
        echo $html;
    }
};
?>
