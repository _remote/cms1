<?php

class m141030_144649_start extends CDbMigration {
    public function up() {
        $sqlFile = dirname(__FILE__)."/start.sql";
        $sql = file_get_contents($sqlFile);
            $this->execute($sql);
    }

    public function down() {
        $this->dropTable('tbl_admin');
        $this->dropTable('tbl_admins_session');
        $this->dropTable('tbl_article');
        
        $this->dropTable('tbl_images');
        $this->dropTable('tbl_language');
        $this->dropTable('tbl_menu');
        $this->dropTable('tbl_page');
        $this->dropTable('tbl_resources');
    }
}