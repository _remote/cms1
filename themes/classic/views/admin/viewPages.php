<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => $this->adminPath,
	Yii::t('page', 'View pages'),
);
?>

<h1><?= Yii::t('page', 'View site pages') ?></h1>
<?php
$models = Language::model()->findAll(array('order' => 'id'));
$list = CHtml::listData($models, 'id', 'name');
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'menu-form-filter',
    'method'=>'get',
    'enableAjaxValidation'=>true,
    'action'=>Yii::app()->createUrl($this->adminPath.'/pages'),
));
echo $form->labelEx($model,Yii::t('main', "Filter:"))."<br>";
echo $form->dropDownList($model, 'id_language', $list, array("style" => "width: 170px;", "empty" => Yii::t('main', "Select language filter")));
echo CHtml::submitButton(Yii::t('main', 'Set filter')); 
$this->endWidget();

$columns = array(
    array(
        'name'=>'id',
        'htmlOptions' => array('style' => 'width: 30px;'),
        'filter' => false,
    ),
    
    array(
        'name' => 'article_search',
        'htmlOptions' => array('style' => 'width: 100px;'),
        'type'=>'html',
        'value' => 'CHtml::link(CHtml::encode($data->article->path), '
        . 'array("'.$this->adminPath.'/article/".$data->article->id))',
    ),
    array(
        'name'=>'title',
        'type'=>'html',
        'value' => 'CHtml::tag("span", array("title"=>$data["keywords"]), CHtml::link(CHtml::encode($data->title), '
        . 'array("'.$this->adminPath.'/page/".$data->id."/'.$model->id_language.'")))',
        'htmlOptions' => array('style' => 'width: 310px;'),
    ),
//    array(
//        'name'=>'keywords',
//        'htmlOptions' => array('style' => 'width: 150px;'),
//        'type'=>'raw',
//        'value' => '( strlen($data["keywords"]) > 10
//            ? CHtml::tag("span", array("title"=>$data["keywords"]), CHtml::encode(substr($data["keywords"], 0, 10)) . "..")
//            : CHtml::encode($data["keywords"])
//        );',
//    ),
    array(
        'name' => 'language_search',
        'type'=>'html',
        'value' => 'CHtml::link(CHtml::encode($data->language->name), '
        . 'array("'.$this->adminPath.'/language/".$data->language->id))',
        'filter' => false,
    ),
    array(
        'name'=>'create_date',
        'filter' => false,
    ),
    array(
        'name'=>'update_date',
        'filter' => false,
    ),
    array(
        'name'=>'is_show',
        'header' => Yii::t('page', 'Show'),
        'type'=>'raw',
        'value' => '!empty($data->is_show)?"<span style=\"color: green;\">'.Yii::t('main', "yes").'</span>":"'.Yii::t('main', "no").'"',
        'filter' => false,
    ),
    array(
        'name'=>'doc_file',
        'header' => Yii::t('page', 'File'),
        'type'=>'raw',
        'value' => '!empty($data->doc_file)?"<span style=\"color: green;\">'.Yii::t('main', "yes").'</span>":"'.Yii::t('main', "no").'"',
        'filter' => false,
    ),
//    array(
//        'name'=>'update_date',
//        'filter' => false,
//    ),
    array(
        'class'=>'CButtonColumn',
        'template'=>'{view} {update} {delete}',
        'buttons'=>array
        (
            'view' => array (
                'url'=>'Yii::app()->createUrl(Yii::app()->baseUrl."/".$data->language->type."/".$data->article->path)',
                'options'=>array("target"=>"_blank"),
            ),
            'update' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/".strtolower(get_class($data))."/$data->id/'.$model->id_language.'")',
            ),
            'delete' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/delete/".strtolower(get_class($data))."/$data->id")',
            ),
        ),
    ),
);

$this->widget('zii.widgets.grid.CGridView', array(
    'enablePagination' => true,
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>$columns,
    'pager'=>array(
        'pageSize' => 20,
    ),
));



?>

<div class="_button_row">
    <div class="_button" onclick="location='<?= $this->adminPath ?>/page'"><?= Yii::t('page', 'Add new page') ?></div>
</div>

