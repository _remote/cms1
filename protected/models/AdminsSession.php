<?php

/**
 * This is the model class for table "{{admins_session}}".
 *
 * The followings are the available columns in table '{{admins_session}}':
 * @property integer $id
 * @property integer $id_admin
 * @property string $session_id
 * @property string $ip
 * @property string $user_agent
 * @property string $open_date
 * @property string $update_date
 * @property integer $is_closed
 */
class AdminsSession extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{admins_session}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_admin, session_id, ip', 'required'),
			array('id_admin, is_closed', 'numerical', 'integerOnly'=>true),
			array('session_id', 'length', 'max'=>32),
			array('ip', 'length', 'max'=>72),
			array('user_agent', 'length', 'max'=>100),
			array('open_date, update_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_admin, session_id, ip, user_agent, open_date, update_date, is_closed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('admin', 'ID'),
			'id_admin' => Yii::t('admin', 'Id Admin'),
			'session_id' => Yii::t('admin', 'Session'),
			'ip' => Yii::t('admin', 'Ip'),
			'user_agent' => Yii::t('admin', 'User Agent'),
			'open_date' => Yii::t('admin', 'Open Date'),
			'update_date' => Yii::t('admin', 'Close Date'),
			'is_closed' => Yii::t('admin', 'Is Closed'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_admin',$this->id_admin);
		$criteria->compare('session_id',$this->session_id,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('user_agent',$this->user_agent,true);
		$criteria->compare('open_date',$this->open_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('is_closed',$this->is_closed);

		return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination' => array(
                        'pageSize' => 10,
                    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdminsSession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
