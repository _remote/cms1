<?php
return array(
    "n#Add new variable|!n#Edit {variable} variable" => "n#Добавить новую константу|!n#Радактировать константу {variable}",
    "n#Add new variable|!n#View '{variable}' variable" => "n#Добавление новой константы|!n#Радактирование константы '{variable}'",
    "n#Set value of variable|!n#Change value of variable" => "n#Задать значение константы|!n#Изменить значение константы",
    "n#Set value of variable '{variable}'|!n#Change value of '{variable}'" => "n#Задать значени константы '{variable}'|!n#Изменить значение константы '{variable}'",
    "View variables" => "Таблица языковых констант",
    "Add variable" => "Добавить константу",
    "Id Language" => "Язык",
    "Id Variable" => "Переменная",
    "Value" => "Значение",
    "Language" => "Язык",
    "Name" => "Название",
    "Comment" => "Комментарий",
    "" => "",
    "" => "",
);

