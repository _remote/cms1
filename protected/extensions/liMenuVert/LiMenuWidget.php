<?php
class LiMenuWidget extends CInputWidget {

    public $items = array();
    public $options;
    public $theme = "blue";
    public $debug = true;
    
    private $assets = "";

    public function run() {
        $this->options['delayShow'] = 300;
        $this->options['delayHide'] = 500;
        
        list($name, $id) = $this->resolveNameID();

        // Publishing assets.
        $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__), false, -1, $this->debug);

        // Registering javascript.
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        
        $time = time();
        $cs->registerScriptFile($this->assets . '/assets/js/jquery.liMenuVert.js');
        $cs->registerCssFile($this->assets . '/assets/css/liMenuVert.css');
        $cs->registerCssFile($this->assets . '/assets/css/liMenuVertTheme-'.strtolower($this->theme).'.css?t='.$time);

        
        $cs->registerScript(
            'Yii.' . get_class($this) . '#' . $id, '$(function(){$("#' . $id . '").liMenuVert(' . CJavaScript::encode($this->options) . ');});'
        );

        $this->htmlOptions = array_merge(array("class" => "menu_vert", "id" => $id, "name" => $name), $this->htmlOptions);
        
        $this->menuHtml($this->items, $this->htmlOptions);
        
    }
    
    protected function menuHtml($items = array(), $htmlOptions = array(), $level = 0) {
        $options = "";
        foreach ($htmlOptions as $key => $option)
            $options .= " {$key}=\"{$option}\"";
        echo "<ul{$options}>";
        foreach ($items as $key => $item) {
            echo "<li>";
            if (is_array($item)) {
                if (isset($item['label'])) {
                    $img = (!empty($item['is_new']))?"<img src=\"{$this->assets}/assets/css/images/b_new.gif\" style=\"vertical-align: middle;\">":"";
                    if (isset($item['url']))
                        echo "<a href=\"{$item['url']}\">{$img} {$item['label']}</a>";
                    else echo "<span>{$item['label']}</span>";
                }
                if (isset($item['items'])) {
                    $sub_items = (is_array($item['items']))?$item['items']:array("0" => $item['items']);
                    $this->menuHtml($sub_items, array(), ++$level);
                }
            }
            else echo "<span class=\"hr\"></span>";
            echo "</li>";
        }
        echo "</ul>";
    }
};
?>
