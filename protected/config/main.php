<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$db = require(dirname(__FILE__).'/db'.(defined('YII_LOCAL')?"_local":"").'.php');
return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Центр тибетской медицины "Ваджра Гаруда"',

    // preloading 'log' component
    'preload'=>array('log'),
    'sourceLanguage'    =>'en_US',
    'language'          =>'ru_RU',
    'theme'=>'classic',

    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.include.*',
        'application.vendors.*',
    ),
    
    'sourceLanguage'=>'en',
    'language'=>'ru',

    'defaultController'=>'site',
    
    'modules'=>array(
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'111111',
            // 'ipFilters'=>array(…список IP…),
            // 'newFileMode'=>0666,
            // 'newDirMode'=>0777,
        ),
    ),
    
//    'session' => array(
//        'sessionName' => 'sid',
//        'timeout' => 24*3600,
//    ),
//    
//    'request'=>array(
//        'enableCookieValidation'=>true,
//    ),
    
    // application components
    'components'=>  array_merge($db, array(
            'user'=>array(
                // enable cookie-based authentication
                'allowAutoLogin'=>true,
            ),
            'errorHandler'=>array(
                // use 'site/error' action to display errors
                'errorAction'=>'site/error',
            ),
            'urlManager'=>array(
                'urlFormat'=>'path',
                //'urlSuffix'=>'.php',
                'showScriptName'=> false, 
                'rules'=>array(
                    'gii'=>'gii',
                    'gii/<controller:\w+>'=>'gii/<controller>',
                    'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
                    
                    '/'=>'site/index/path/index',
                    '<language:\w{2}>/' => 'site/index/path/index',
                    '<language:\w{2}>/<path:.*?>'=>'site/index',
                    'comment' => 'site/comment',
                    
                    'setlocale/<language:\w{2}>' => '/site/setlanguage',
                    'setlocale/<language:\w{2}>/<from:\w+>' => '/site/setlanguage',
                    
                    'ed.panel/admin/<id:\d+>' => '/admin/add',
                    'ed.panel/language/<id:\d+>' => '/admin/language',
                    'ed.panel/article/<id:\d+>' => '/admin/article',
                    'ed.panel/menu/<id:\d+>' => '/admin/menu',
                    'ed.panel/menu/<id:\d+>/<id_language:\d+>' => '/admin/menu',
                    'ed.panel/page/<id:\d+>' => '/admin/page',
                    'ed.panel/page/<id:\d+>/<id_language:\d+>' => '/admin/page',
                    'ed.panel/image/<id:\d+>' => '/admin/image',
                    'ed.panel/video/<id:\d+>' => '/admin/video',
                    'ed.panel/menus/<id_language:\d+>' => '/admin/menus',
                    'ed.panel/pages/<id_language:\d+>' => '/admin/pages',
                    'ed.panel/<action:\w+>'=>'/admin/<action>',
                    'ed.panel/delete/<model:\w+>/<id:\d+>'=>'/admin/delete',
                    'ed.panel/sortmenu/<lid:\d+>' => '/admin/sortmenu',
                    
                    'ed.panel/resource/<id:\d+>' => '/admin/resource',
                    'ed.panel/resource/<id_language:\d+>/<id_variable:\d+>' => '/admin/resource',
                    'ed.panel/resourcevar/<id:\d+>' => '/admin/resourcevar',
                    
                    'ed.panel/approvecomment/<id:\d+>' => '/admin/approvecomment',
                    'ed.panel/comment/<id:\d+>' => '/admin/comment',
                    'ed.panel/comments/<id_language:\d+>' => '/admin/comments',
                    //'ed.panel/error'=>'/admin/error',
                    'ed.panel/'=>'/admin/index',

                    '<path:.*?>'=>'site/index/path/<path>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ),
            ),
            'preload'=>array('log'),
            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CWebLogRoute',
                        'levels'=>'trace',
                        //'showInFireBug'=>true,
                    ),
                ),
            ),

        )
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>require(dirname(__FILE__).'/params.php'),
);