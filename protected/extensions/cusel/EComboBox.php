<?php
class EComboBox extends CInputWidget {

    public $data;
    public $options;

    public function run() {
        list($name, $id) = $this->resolveNameID();
        
        $this->options['changedEl'] = "#".$name;
        $this->options['visRows'] = 10;
        $this->options['scrollArrows'] =  true;

        // Publishing assets.
        $assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__));

        // Registering javascript.
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        
        $time = time();
        $cs->registerScriptFile($assets . '/assets/js/cusel.js');
        $cs->registerScriptFile($assets . '/assets/js/jscrollpane.js');
        $cs->registerCssFile($assets . '/assets/cusel.css');

        $cs->registerScript(
            'Yii.' . get_class($this) . '#' . $id, '$(function(){cuSel(' . CJavaScript::encode($this->options) . ');});'
        );
        //print_r(CJavaScript::encode($this->options));
        //if ($this->hasModel()) {
            $html = CHtml::dropDownList($name, null, $this->data, $this->htmlOptions);
        //} else {
        //    $html = CHtml::textArea($name, $this->value, $this->htmlOptions);
        //}
        echo $html;
    }
};
?>
