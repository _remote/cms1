<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'], Yii::t('admin', 'Change user password'));
?>

<h1><?= Yii::t('admin', 'Please write new password') ?></h1>
<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'user-password-form',
        //'action' => Yii::app()->params['adminPath']."/password",
        'enableAjaxValidation'=>false,
    )); ?>
    
    <?php if (!empty($error)) { ?>
    <div id="user-password-form_es_" class="errorSummary">
        <p><?= $error ?></p>
    </div>
    <?php } ?>
    
    <div class="row">
        <?php echo Chtml::label(Yii::t('admin', 'New password'), 'password'); ?>
        <?php echo Chtml::textField('password', "", array("style" => "width: 200px;")); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('main', 'Set')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

