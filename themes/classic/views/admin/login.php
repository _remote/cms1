<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(Yii::t('main', 'Admin') => "index", 
    Yii::t('main', 'Login in panel'),
);
?>

<h1><?= Yii::t('main', 'Authorization form') ?></h1>

<!--<p>Please fill out the following form with your login credentials:</p>-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>true,
)); ?>

	<div class="row">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
		<p class="hint">
			<?= Yii::t('main', 'Hint: You may contact to') ?> <tt><?= Yii::app()->params['adminEmail'] ?></tt> <?= Yii::t('main', 'for more detalies') ?>.
		</p>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton(Yii::t('main', 'Login ->')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
