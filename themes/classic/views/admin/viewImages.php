<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
    Yii::t('image', 'View photos'),
);
?>

<h1><?= Yii::t('image', 'View site images') ?></h1>
<?php
$columns = array(
    'id' => array(
        'name' => 'id',
        'htmlOptions' => array('style' => 'width: 30px; text-align: center;'),
    ),
    array(
        'name' => 'article_search',
        'htmlOptions' => array('style' => 'width: 100px;'),
        'type'=>'html',
        'value' => '(isset($data->article->id))?CHtml::link(CHtml::encode($data->article->path), '
        . 'array("'.$this->adminPath.'/article/".$data->article->id)):"null"',
    ),
    'name',
    'image' => array(
        'name'=>'image',
        'type'=>'html',
        'htmlOptions' => array('align' => 'center', 'style' => 'width: 110px;'),
        'value'=>'(!empty($data->path))?CHtml::image($data->path."?t='.time().'", "", array("style"=>"width:100px;margin: 0px 10px;")):"no image"',

    ),
    'title',
    array(
        'class'=>'CButtonColumn',
        'template'=>'{update} {view} {delete}',
        'buttons'=>array
        (
            'view' => array (
                'url'=>'Yii::app()->createUrl(Yii::app()->baseUrl.$data->path)',
                'options'=>array("target"=>"_blank"),
            ),
            'update' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/".strtolower(get_class($data))."/$data->id")',
            ),
            'delete' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/delete/".strtolower(get_class($data))."/$data->id")',
            ),
        ),
    ),
);
$this->widget('zii.widgets.grid.CGridView', array(
    'enablePagination' => true,
    'dataProvider'=> new CActiveDataProvider($model),
    'columns'=>$columns,
));
?>

<div class="_button_row">
    <div class="_button" onclick="location='<?= $this->adminPath ?>/image'"><?= Yii::t('image', 'Add photo') ?></div>
</div>

