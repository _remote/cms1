<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class AdminLoginForm extends CFormModel {
	public $username;
	public $password;
	public $rememberMe;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
            return array(
                // username and password are required
                array('username, password', 'required'),
                // rememberMe needs to be a boolean
                array('rememberMe', 'boolean'),
                // password needs to be authenticated
                array('password', 'authenticate'),
            );
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
            return array(
                'rememberMe'=>Yii::t('admin', 'Remember me next time'),
            );
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params) {
            $this->_identity=new AdminIdentity($this->username,$this->password);
            if(!$this->_identity->authenticate())
                $this->addError('password',Yii::t('admin', 'Incorrect username or password.'));
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login() {
            if($this->_identity===null) {
                $this->_identity=new AdminIdentity($this->username,$this->password);
                $this->_identity->authenticate();
            }
            if($this->_identity->errorCode===AdminIdentity::ERROR_NONE) {
                $duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
                Yii::app()->user->login($this->_identity,$duration);
                //Закрываем все предыдущие открытые сессии
                AdminsSession::model()->updateAll(array("update_date" => date("Y-m-d G:i:s"), "is_closed" => 1), "is_closed = 0");
                $session = new AdminsSession;
                $session->id_admin = $this->_identity->getId();
                $session->session_id = Yii::app()->session->sessionID;
                $session->ip = @$_SERVER['REMOTE_ADDR'];
                $session->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                $session->open_date = @date("Y-m-d G:i:s", time());
                if ($session->validate())
                    if (!$session->save())
                        return false;
                return true;
            } 
            else return false;
	}
}
