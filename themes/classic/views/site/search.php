<?php
$this->pageTitle=Yii::app()->name . ' - '.$this->getResourcesValue("search_result");

$content = <<<STR
    <div class="search_item_header">
        <a href="{LINK}">{TITLE}</a>
    </div>
    <div class="search_item">{CONTENT}</div>
STR;

function searchColor($str = "", $search = "") {
//    $a1 = mb_convert_case($str, MB_CASE_LOWER, "UTF-8");
//    $a2 = mb_convert_case($search, MB_CASE_LOWER, "UTF-8");
//    $position = strpos($a1, $a2);
//    $length = strlen($search);
//    $find = substr($str, $position, $length);
    $find = mb_convert_case($search, MB_CASE_UPPER, "UTF-8");
    $result = preg_replace("/{$search}/iu", "<b style=\"color: #366097;\">{$find}</b>", $str);
    return $result;
}

function getSentence($text = "", $search = "", $count = 3) {
    $i = 0;
    $str = "";
    $substr = explode(".", $text);
    foreach ($substr AS $key => $value){
        if ($value != "") {
            if (preg_match("/{$search}/iu", $value) || $i > 0) {
                $str .= $value."."; //одно предложение
                $i++;
            }
        }
        if ($i >= $count) return $str;
    }
    return $str;
}
?>
<h1><?= $this->getResourcesValue("search_result") ?></h1>
<div class="body"> 
    <?php
    if ($results) {
        foreach ($results as $item) {
            $str = strip_tags($item->content);
            $sentence = getSentence($str, $search);
            $result = searchColor($sentence, $search);
            $html = str_replace("{TITLE}", $item->title, $content);
            $html = str_replace("{LINK}", $item->article->path, $html);
            echo str_replace("{CONTENT}", $result, $html);
        }
    }
    else echo $this->getResourcesValue("empty_search");
    ?>
    
</div>

