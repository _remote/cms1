<?php

/**
 * This is the model class for table "{{comment}}".
 *
 * The followings are the available columns in table '{{comment}}':
 * @property integer $id
 * @property integer $id_language
 * @property string $name
 * @property string $email
 * @property string $text
 * @property string $create_date
 * @property integer $is_approved
 */
class Comment extends CActiveRecord {
    
    public $language_search;
    public $verifyCode;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{comment}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('text', 'required'),
			array('id_language, is_approved', 'numerical', 'integerOnly'=>true),
			array('name, email', 'length', 'max'=>50),
			array('create_date', 'safe'),
                        array('email' , 'email'),
                        array('verifyCode', 'application.extensions.recaptcha.EReCaptchaValidator', 'privateKey'=>'6LejOP4SAAAAALlAjIODmjnTH2pH9Dji8z2-gHiC', 'on' => 'registerwcaptcha'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_language, name, email, text, create_date, is_approved, language_search', 'safe', 'on'=>'search'),
		);
	}
        

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'language' => array(self::BELONGS_TO, 'Language', 'id_language'),
            );
	}
        
        

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
            return array(
                'id' => Yii::t('comment', 'ID'),
                'id_language' => Yii::t('comment', 'Id Language'),
                'name' => Yii::t('comment', 'Name'),
                'email' => Yii::t('comment', 'Email'),
                'text' => Yii::t('comment', 'Comment'),
                'create_date' => Yii::t('comment', 'Create Date'),
                'is_approved' => Yii::t('comment', 'Is Approved'),
                'language_search' => Yii::t('comment', 'Language'),
                'verifyCode' => Yii::t('comment', 'Verify Code'),
            );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array( 'language' );

		$criteria->compare('id',$this->id);
		$criteria->compare('id_language',$this->id_language);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('is_approved',$this->is_approved);

		$criteria->compare( 'language.name', $this->language_search, true );

		return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'sort'=>array(
                        'attributes'=>array(
                            'language_search'=>array(
                                'asc'=>'language.name',
                                'desc'=>'language.name DESC',
                            ),
                            '*',
                        ),
                    ),
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['pageSize'],
                    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
