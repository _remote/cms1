<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
    Yii::t('main', 'Photos') => Yii::app()->params['adminPath']."/images",
    Yii::t('image', 'n#Add new photo|!n#View {photo} photo', array($model->isNewRecord, "{photo}" => $model->name))
);
?>
<h1><?= Yii::t('image', 'n#Add new photo|!n#View \'{photo}\' photo', array($model->isNewRecord, "{photo}" => $model->name)) ?></h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'image-image-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>true,
        //'action' => Yii::app()->params['adminPath']."/imageUpload",
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note"><?= Yii::t('main', 'Fields with') ?> <span class="required">*</span> <?= Yii::t('main', 'are required') ?>.</p>

	<?php echo $form->errorSummary($model); ?>

        <?php echo Chtml::hiddenField("_add", 1); ?>
	<div class="row">
            <?php echo $form->labelEx($model,'name'); ?>
            <?php echo $form->textField($model,'name'); ?>
            <?php echo $form->error($model,'name'); ?>
	</div>
        
        <?php if ($model->isNewRecord) { ?>
        <div class="row">
            <?php echo $form->labelEx($model,'image'); ?>
            <?php echo $form->fileField($model,'image'); ?>
            <?php echo $form->error($model,'image'); ?>
        </div>
        <?php } ?>
        
        <div class="row">
            <?php $list = Article::model()->getList(); ?>
            <?php echo $form->labelEx($model,Yii::t('image', 'Article path')); ?>
            <?php echo $form->dropDownList($model, 'id_article', $list, array("style" => "width: 300px;")); ?>
            <?php echo $form->error($model,'id_article'); ?>
	</div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'title'); ?>
            <?php echo $form->textField($model,'title'); ?>
            <?php echo $form->error($model,'title'); ?>
	</div>
        
        <?php if (!$model->isNewRecord) { ?>
        <div class="row">
            <?php echo $form->labelEx($model,'path'); ?>
            <?php echo $form->textField($model,'path', array("disabled" => "disabled")); ?>
            <?php echo Chtml::link(CHtml::image("/images/view.png"), $model->path, array("target" => "_blank"))?>
            <?php echo $form->error($model,'path'); ?>
	</div>
        
	<div class="row">
            <?php echo $form->labelEx($model,'width'); ?>
            <?php echo $form->textField($model,'width'); ?>
            <?php echo $form->error($model,'width'); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'height'); ?>
            <?php echo $form->textField($model,'height'); ?>
            <?php echo $form->error($model,'height'); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'create_date'); ?>
            <?php echo $form->textField($model,'create_date'); ?>
            <?php echo $form->error($model,'create_date'); ?>
	</div>
        
        <?php } ?>


	<div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('main', 'n#Create|!n#Save', array($model->isNewRecord))); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->