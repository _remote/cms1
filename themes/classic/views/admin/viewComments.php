<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => $this->adminPath,
    Yii::t('comment', 'View users comments'),
);
?>

<h1><?= Yii::t('comment', 'Table of users comments') ?></h1>
<?php
$models = Language::model()->findAll(array('order' => 'id'));
$list = CHtml::listData($models, 'id', 'name');
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'menu-form-filter',
    'method'=>'get',
    'enableAjaxValidation'=>true,
    'action'=>Yii::app()->createUrl($this->adminPath.'/comments'),
));
echo $form->labelEx($model,Yii::t('main', "Filter:"))."<br>";
echo $form->dropDownList($model, 'is_approved', array("" => Yii::t('main', "By approved"), "0" => Yii::t('main', "no"), "1" => Yii::t('main', "yes")), array("style" => "width: 170px;"))."<br>";
echo $form->dropDownList($model, 'id_language', $list, array("style" => "width: 170px;", "empty" => Yii::t('main', "Select language filter")));
echo "<br>".CHtml::submitButton(Yii::t('main', 'Set filter')); 
$this->endWidget();

$columns = array(
    array(
        'name'=>'id',
        'filter' => false,
        'htmlOptions' => array('style' => 'width: 50px;'),
    ),
    array(
        'name' => 'language_search',
        'type'=>'html',
        'value' => 'CHtml::link(CHtml::encode($data->language->name), '
        . 'array("'.$this->adminPath.'/language/".$data->language->id))',
        'filter' => false,
        'htmlOptions' => array('style' => 'width: 100px;'),
    ),
    array(
        'name'=>'name',
        'htmlOptions' => array('style' => 'width: 100px;'),
    ),
    array(
        'name'=>'text',
    ),
    
    array(
        'name'=>'is_approved',
        'header' => Yii::t('comment', 'Is Approved'),
        'type'=>'raw',
        'value' => '!empty($data->is_approved)?"<span style=\"color: green;\">'.Yii::t('main', "yes").'</span>":"'.Yii::t('main', "no").'"',
        'filter' => false,
        'htmlOptions' => array('style' => 'width: 100px;'),
    ),
    array(
        'class'=>'CButtonColumn',
        'template'=>'{approve} {update} {delete}',
        'buttons'=>array
        (
            'update' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/".strtolower(get_class($data))."/$data->id/'.$model->id_language.'")',
            ),
            'delete' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/delete/".strtolower(get_class($data))."/$data->id")',
            ),
            'approve' => array(
                'imageUrl' => '/images/tick.png',
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/approvecomment/$data->id")',
                'visible' => 'empty($data->is_approved)',
            ),
        ),
    ),
);

$this->widget('zii.widgets.grid.CGridView', array(
    'enablePagination' => true,
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>$columns,
    'pager'=>array(
        'pageSize' => 20,
    ),
));

?>

<div class="_button_row">
    <div class="_button" onclick="location='<?= $this->adminPath ?>/comment?id_language=<?= $model->id_language ?>'"><?= Yii::t('comment', 'Add comment') ?></div>
</div>

