<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
        Yii::t('main', 'Videos') => Yii::app()->params['adminPath']."/videos",
        Yii::t('video', 'n#Add new video|!n#View video', array($model->isNewRecord)),
);
?>

<h1><?= Yii::t('video', 'n#Add new video|!n#View video', array($model->isNewRecord)) ?></h1>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'video-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?= Yii::t('main', 'Fields with') ?> <span class="required">*</span> <?= Yii::t('main', 'are required') ?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'html_code'); ?>
		<?php echo $form->textArea($model,'html_code',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'html_code'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('main', 'n#Create|!n#Save', array($model->isNewRecord))); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->