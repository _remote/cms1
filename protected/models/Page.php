<?php

/**
 * This is the model class for table "{{page}}".
 *
 * The followings are the available columns in table '{{page}}':
 * @property integer $id
 * @property integer $id_article
 * @property integer $id_language
 * @property string $title
 * @property string $content
 * @property string $keywords
 * @property string $doc_file
 * @property integer $is_show
 * @property string $create_date
 * @property string $update_date
 */
class Page extends CActiveRecord {
    
        public $article_search;
        public $language_search;
        public $_file;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
            return '{{page}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                //array('keywords', 'required'),
                array('id_article, id_language, is_show', 'numerical', 'integerOnly'=>true),
                array('title, keywords', 'length', 'max'=>512),
                array('doc_file', 'length', 'max'=>100),
                array('content, create_date, update_date', 'safe'),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id_article','uniqueLanguageWithArticle', 'message' => Yii::t('page', 'For this language already has such article')),
                array('id, id_article, id_language, title, content, keywords, doc_file, is_show, create_date, update_date, article_search, language_search', 'safe', 'on'=>'search'),
                array('_file', 'file', 'types'=>'doc, docx, xls, xlsx', 'message' => Yii::t('page', 'Please choose document.'), 'allowEmpty'=>true),
            );
	}
        
        public function uniqueLanguageWithArticle($attribute,$params=array()) {
            //if(!$this->hasErrors() || $this->isNewRecord) {
                $params['criteria']=array(
                    'condition'=>'id_language=:id_language',
                    'params'=>array(':id_language'=>$this->id_language),
                );
                $validator=CValidator::createValidator('unique',$this,$attribute,$params);
                $validator->validate($this,array($attribute));
            //}
        } 

	/**
	 * @return array relational rules.
	 */
	public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'article' => array(self::BELONGS_TO, 'Article', 'id_article'),
                'language' => array(self::BELONGS_TO, 'Language', 'id_language'),
            );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
            return array(
                'id' => Yii::t('page', 'ID'),
                'id_article' => Yii::t('page', 'Id Article'),
                'id_language' => Yii::t('page', 'Id Language'),
                'title' => Yii::t('page', 'Title'),
                'content' => Yii::t('page', 'Content'),
                'keywords' => Yii::t('page', 'Keywords'),
                'doc_file' => Yii::t('page', 'Doc File'),
                'is_show' => Yii::t('page', 'Is Show'),
                'create_date' => Yii::t('page', 'Create Date'),
                'update_date' => Yii::t('page', 'Update Date'),
                'article_search' => Yii::t('page', 'Link'),
                'language_search' => Yii::t('page', 'Language'),
                '_file' => Yii::t('page', 'Choose document'),
            );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.
            $criteria=new CDbCriteria;
            $criteria->with = array('article', 'language' );

            $criteria->compare('id',$this->id);
            $criteria->compare('id_article',$this->id_article);
            $criteria->compare('id_language',$this->id_language);
            $criteria->compare('title',$this->title,true);
            $criteria->compare('content',$this->content,true);
            $criteria->compare('keywords',$this->keywords,true);
            $criteria->compare('doc_file',$this->doc_file,true);
            $criteria->compare('is_show',$this->is_show);
            $criteria->compare('create_date',$this->create_date,true);
            $criteria->compare('update_date',$this->update_date,true);

            $criteria->compare( 'article.path', $this->article_search, true );
            $criteria->compare( 'language.name', $this->language_search, true );

            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort'=>array(
                    'attributes'=>array(
                        'article_search'=>array(
                            'asc'=>'article.path',
                            'desc'=>'article.path DESC',
                        ),
                        'language_search'=>array(
                            'asc'=>'language.name',
                            'desc'=>'language.name DESC',
                        ),
                        '*',
                    ),
                ),
                'pagination' => array(
                        'pageSize' => Yii::app()->params['pageSize'],
                ),
            ));
	}
        
        protected function beforeSave() {
            $this->create_date = @date("Y-m-d G:i:s", time());
            if(!parent::beforeSave())
                return false;
            return true;
        }
        
        protected function beforeDelete(){
            if(!parent::beforeDelete())
                return false;
            $this->deleteDocument(); // удалили модель? удаляем и файл
            return true;
        }

        public function deleteDocument(){
            $documentPath=Yii::getPathOfAlias('webroot.upload').$this->doc_file;
            if(is_file($documentPath))
                unlink($documentPath);
        }
        
        public function countItemsForLanguage($id_language = false) {
            $_id = ($id_language)?$id_language:$this->id_language;
            return ($_id)?($this->model()->count("id_language = :id_language", array(":id_language" => $_id))):0;
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}
