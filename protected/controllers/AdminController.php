<?php

class AdminController extends Controller {
    
    public $sys_apache_version;
    public $role;
    public $session_ip;
    public $derictories = array();
    private $_model;
    
    public function init(){
        parent::init();
        
        Yii::app()->language = "ru";
               
        $this->sys_apache_version = $this->apache_version();
        
        $this->breadcrumbs = array('Admin' => array(Yii::app()->params['adminPath']));
        $this->layout = '//layouts/admin/main';
        if (Yii::app()->user->isGuest) {
            $this->actionLogin();
            Yii::app()->end();
        }
        else {
            $criteria = new CDbCriteria(array(
                'condition' => "id_admin = :id_admin AND `session_id` = :session_id AND `is_closed` = 0",
                'params'    => array(":id_admin" => Yii::app()->user->id, ":session_id" => Yii::app()->session->sessionID),
                'order' => 'update_date DESC',
            ));
            $session = AdminsSession::model()->find($criteria);
//            if (!$session || 
//                    ($session && ($session->is_closed/* || 
//                            ($session->ip != @$_SERVER['REMOTE_ADDR'] && !in_array($session->ip, array("::1", "127.0.0.1")))*/ ))) { //Если зашли с другого ip, но нужно перелогинется
//                Yii::app()->getSession()->regenerateID();
//                if (!$session->is_closed)
//                    $this->actionLogout();
//            }
            //if (!$session) {
            //    Yii::app()->getSession()->regenerateID();
            //    $this->redirect(Yii::app()->params['adminPath'].'/login');
            //}
            
            
            $user = Admin::model()->findByPk(Yii::app()->user->id);
            $this->role = $user->permission;
        }
    }
    
    public function actionAdd() {
        if (isset($_GET['id'])) $model = Admin::model ()->findByPk (intval($_GET['id']));
        else $model=new Admin;

        // uncomment the following code to enable ajax-based validation
        if(isset($_POST['ajax']) && $_POST['ajax']==='admin-_add-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['Admin'])) {
            $model->attributes=$_POST['Admin'];
            if ($model->isNewRecord) {
                $model->salt = md5(time());
                $model->password = $model->hashPassword($model->password);
                $model->create_date = $this->_now;
            }
            if($model->validate()) {
                if ($model->save())
                    $this->redirect(Yii::app()->params['adminPath']);
            }
        }
        
        $this->render('_add',array('model'=>$model));
    }
    
    /**
	 * Displays the login page
	 */
    public function actionLogin() {
        if (!Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->params['adminPath'].'/index');
        
        if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
            throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

        $model=new AdminLoginForm;
        
        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['AdminLoginForm'])) {
            $model->attributes=$_POST['AdminLoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(Yii::app()->params['adminPath']);
        }
        
        // display the login form
        $this->render('login',array('model'=>$model));
    }

    
    /**
    * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
    */
    public function loadModel() {
        if($this->_model===null) {
            if(isset($_GET['model']) && isset($_GET['id']) && !Yii::app()->user->isGuest) {
                $model = ucfirst($_GET['model']);
                $_model = call_user_func(array($model, 'model'));
                $this->_model=$_model->findByPk($_GET['id']);
            }
            if($this->_model===null)
                throw new CHttpException(404,'The requested page does not exist.');
        }
        return $this->_model;
    }
    
    /**
     * Get apache version
     * @return string version apache
     */
    public function apache_version(){
        if (function_exists('apache_get_version')){
                if (preg_match('|Apache\/(\d+)\.(\d+)\.(\d+)|', apache_get_version(), $version)){
                        return 'Apache v.'.$version[1].'.'.$version[2].'.'.$version[3];
                }
        } elseif (isset($_SERVER['SERVER_SOFTWARE'])){
                if (preg_match('|Apache\/(\d+)\.(\d+)\.(\d+)|', $_SERVER['SERVER_SOFTWARE'], $version)){
                        return 'Apache v.'.$version[1].'.'.$version[2].'.'.$version[3];
                } 
        }
        return '(unknown)';
    }

	/**
     * Filter request
     */
    public function filters(){
        return array(
            array(
                'application.include.XssFilter',
                'clean'   => '*',
                'tags'    => 'soft',
                'actions' => 'all'
            )
        );
    }
    
    private function replace_modxtags($matches){
        $code_entities_match = array('&','[', ']','{', '}','<','>');
        $code_entities_replace = array('&amp;', '&#91;', '&#93;', '&#123;', '&#125;', '&lt;', '&gt;');
        $code = str_replace($code_entities_replace, $code_entities_match, $matches);
        return $code;
    }
     
    public function actionIndex() {
        $my = Admin::model()->findByPk(Yii::app()->user->id);
        $criteria = new CDbCriteria(array(
            'condition' => "id_admin = :id_admin",
            'params'    => array(":id_admin" => Yii::app()->user->id),
            'order' => 'open_date DESC',
        ));
        $session = AdminsSession::model()->find($criteria); 
        if (!$session) $session = new AdminsSession;
        
        $admins = new Admin('search');
        if(isset($_GET['Admin']))
            $admins->attributes=$_GET['Admin'];
        $this->render('index', array("model" => $my, "session" => $session, "admins" => $admins));
    }
    
    public function actionError(){
        if ($error=Yii::app()->errorHandler->error) {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
    
    public function actionPassword() {
        $error = false;
        $user = Admin::model()->findByPk(Yii::app()->user->id);
        if (isset($_POST['password'])) {
            if (empty($_POST['password'])) $error = "Password can`t be blank.";
            else {
                $user->password = crypt($_POST['password'], md5($user->salt."_".$user->username));
                if ($user->validate()) {
                    if ($user->save())
                        $this->redirect(Yii::app()->params['adminPath']);
                }
                $error = "Can`t change password. Try again later.";
            }
        }
        $this->render("password", array("error" => $error));
    }
        
	/**
	 * Logs out the current user and redirect to homepage.
	 */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->params['adminPath'].'/login');
    }
    
    public function actionLanguages() {
        $model = new Language('search');
        if(isset($_GET['Language']))
            $model->attributes=$_GET['Language'];
        
        $res = new Resourcevar('search');
        if(isset($_GET['Resourcevar']))
            $res->attributes=$_GET['Resourcevar'];
        $this->render('viewLanguages', array("model" => $model, "resources" => $res));
    }
    
    public function actionLanguage() {
        if (isset($_GET['id'])) $model = Language::model ()->findByPk (intval($_GET['id']));
        else $model= new Language;

        // uncomment the following code to enable ajax-based validation
        if(isset($_POST['ajax']) && $_POST['ajax']==='language-language-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['Language'])) {
            $model->attributes=$_POST['Language'];
            $model->image=CUploadedFile::getInstance($model,'image');
            if ($model->image) {
                $sourcePath = pathinfo($model->image->getName());
                //Array ( [dirname] => . [basename] => back.jpg [extension] => jpg [filename] => back )
                echo $tempFile = 'upload/'.$model->type;
                $model->image->saveAs($tempFile);
                $thumb = PhpThumbFactory::create($tempFile);
                $thumb->adaptiveResize(50, 32);  
                $path = 'upload/language/'.$model->type.'.png';
                $thumb->save($path, 'png');
                $model->icon = "/language/".$model->type.".png";
                $model->image = NULL;
                @unlink($tempFile);
            }
            if($model->validate()) {
                if ($model->is_default)
                    Language::model()->updateAll(array("is_default" => 0));
                if ($model->save()) 
                    $this->redirect (Yii::app()->params['adminPath'].'/languages');
            }
        }
        $this->render('language',array('model'=>$model));
    }
    
    public function actionVideos() {
        $model = new Video('search');
        if(isset($_GET['Video']))
            $model->attributes=$_GET['Video'];
        $this->render('viewVideos', array("model" => $model));
    } 
    
    public function actionVideo() {
        if (isset($_GET['id'])) $model = Video::model ()->findByPk (intval($_GET['id']));
        else $model=new Video;

        // uncomment the following code to enable ajax-based validation
        if(isset($_POST['ajax']) && $_POST['ajax']==='video-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['Video'])) {
            $model->attributes=$_POST['Video'];
            //$model->id_autor = Yii::app()->user->id;
            if ($model->isNewRecord)
                $model->create_date = $this->_now;

            if($model->validate()) {
                if ($model->save()) 
                    $this->redirect (Yii::app()->params['adminPath'].'/videos');
            }
        }
        else $model->html_code = htmlspecialchars_decode ($model->html_code);
        $this->render('video',array('model'=>$model));
    }
    
    public function actionArticles() {
        $model = new Article('search');
        if(isset($_GET['Article']))
            $model->attributes=$_GET['Article'];
        $this->render('viewArticles', array("model" => $model));
    }   
    
    public function actionArticle() {
        if (isset($_GET['id'])) $model = Article::model ()->findByPk (intval($_GET['id']));
        else $model=new Article;

        // uncomment the following code to enable ajax-based validation
        if(isset($_POST['ajax']) && $_POST['ajax']==='article-article-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['Article'])) {
            $model->attributes=$_POST['Article'];
            $model->id_autor = Yii::app()->user->id;
            if ($model->isNewRecord)
                $model->create_date = $this->_now;
            else $model->update_date = $this->_now;
            $model->path = strtolower($model->path);
            if($model->validate()) {
                if ($model->save()) 
                    $this->redirect (Yii::app()->params['adminPath'].'/articles');
            }
        }
        $this->render('article',array('model'=>$model));
    }
    
    public function actionMenus() {
        $model = new Menu('search');
        if(isset($_GET['Menu']))
            $model->attributes=$_GET['Menu'];
        if (isset($_GET['id_language']))
            $model->id_language = intval($_GET['id_language']);
        $lid = (!empty($model->id_language))?$model->id_language:false;
        //if (!$lid && ($l = Language::model()->find())) 
        //    $lid = $l->getAttribute ("id_language");
        $this->render('viewMenu', array("model" => $model, "lid" => $lid));
    } 
    
    
    
    public function actionMenu() {
        if (isset($_GET['id'])) $model = Menu::model ()->findByPk (intval($_GET['id']));
        else $model=new Menu;

        // uncomment the following code to enable ajax-based validation
        if(isset($_POST['ajax']) && $_POST['ajax']==='menu-menu-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['Menu'])) {
            $model->attributes=$_POST['Menu'];
            if ($model->isNewRecord) {
                $model->position = 0;
                //$model->is_new = 1;
            }
            if($model->validate()) {
                if ($model->save()) 
                    $this->redirect ($this->adminPath.'/menus'.(isset($_POST['lid'])?"/".intval($_POST['lid']):""));
            }
        }
        
        $language = isset($_GET['id_language'])?Language::model()->findByPk(intval($_GET['id_language'])): false;
        $this->render('menu',array('model'=>$model, 'language' => $language));
    }
    
    private function emptyElement($item) {
        return !empty($item);
    }
    
    public function actionSortMenu() {
        if (isset($_GET['lid']))
            $lang = Language::model()->findByPk(intval($_GET['lid']));
        else $lang = Language::model()->find();
        
        if (!empty($_POST['data'])) {
            $items = array_filter($_POST['data'], function($el){ return !empty($el);});
            foreach ($items as $id => $p) {
                Menu::model()->updateByPk($id, array("position" => $p));
            }
            echo json_encode(array("result" => true));
            Yii::app()->end();
        }
        
        $this->render("sortMenu", array("lang" => $lang));
    }
    
    public function actionLoadSubMenu() {
        if (isset($_POST['id'])) {
            $id = intval($_POST['id']);
            $lang = (isset($_POST['lang']))?$this->p->purify($_POST['lang']):"ru";
            $this->widget('zii.widgets.CMenu',array(
                'id' => "sub_".$id,
                'items'=> Menu::model()->loadItems($lang, true, null, $id),
                'htmlOptions' => array('class' => 'sortable', "rel" => $id),
            ));
        }
    }
    
    public function actionTest() {
        $items = Resources::model()->getResourcesList();
        header("Content-Type: text/plain");
        print_r($items);
    }
    
    public function actionPages() {
        $model = new Page('search');
        if(isset($_GET['Page']))
            $model->attributes=$_GET['Page'];
        if (isset($_GET['id_language']))
            $model->id_language = intval($_GET['id_language']);
        $this->render('viewPages', array("model" => $model));
    }
    
    public function actionPage() {
        if (isset($_GET['id'])) $model = Page::model ()->findByPk (intval($_GET['id']));
        else $model=new Page;

        // uncomment the following code to enable ajax-based validation
        if(isset($_POST['ajax']) && $_POST['ajax']==='page-page-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['Page'])) {
            $model->attributes=$_POST['Page'];
            if (!$model->isNewRecord)
                $model->update_date = $this->_now;
            $model->content = $this->replace_modxtags($model->content);
            $model->_file = CUploadedFile::getInstance($model,'_file');
            if ($model->_file) {
                $sourcePath = pathinfo($model->_file->getName());
                //Array ( [dirname] => . [basename] => back.jpg [extension] => jpg [filename] => back )
                $language = Language::model()->findByPk($model->id_language);
                $article = Article::model()->findByPk($model->id_article);
                $path = '/files/'.$language->type."_".$article->path.".".$sourcePath['extension'];
                $model->doc_file = $path;
            }
            if($model->validate()) {
                if ($model->save()) {
                    if ($model->_file)
                        $model->_file->saveAs(Yii::getPathOfAlias('webroot.upload').$path);
                    $this->redirect ($this->adminPath.'/pages'.(isset($_POST['lid'])?"/".intval($_POST['lid']):""));
                }
            }
        }
        
        $language = isset($_GET['id_language'])?Language::model()->findByPk(intval($_GET['id_language'])): false;
        $this->render('page',array('model'=>$model, 'language' => $language));
    }
    
    public function actionLoadPagesMenu() {
        $id = (isset($_POST['id']))?intval($_POST['id']):false;
        if ($id) {
            $model = Page::model()->with(array("article"))->findAll("id_language = :id_language", array(":id_language" => $id), array('order' => 'id'));
            $list = CHtml::listData($model, 'article.path', 'title');
            echo CHtml::dropDownList(@$_POST['selector'], null, $list/*, array("style"=> "width: 100px;")*/);
            Yii::app()->end();
        }
    }
    
    public function actionLoadArticleImages() {
        $id = (isset($_POST['id']))?intval($_POST['id']):false;
        $images = Image::model()->findAll("id_article = :id_article", array(":id_article" => $id));
        if ($images) {
            foreach ($images as $item) {
                $title = ($item->title)?"alt=\"{$item->title}\" title=\"{$item->title}\"":"";
                echo "<li><img src=\"{$item->path}\" {$title} onclick=\"insertImage(this);\"></li>\n"; 
            }
        } 
    }


    public function actionUpload() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder='upload/';// folder for uploaded files
        $allowedExtensions = array("jpg");//array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 1 * 1024 * 1024;// maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $result=htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $result;// it's array
    }
    
    public function actionDelete() {
        if(Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = $this->loadModel()->delete();
            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(array($this->adminPath, strtolower(get_class($model))."s"));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }
    
    public function actionImages() {
        $model = new Image('search');
        if(isset($_GET['Image']))
            $model->attributes=$_GET['Image'];
        $this->render('viewImages', array("model" => $model));
    }
    
    public function actionImage() {
        if (isset($_GET['id'])) $model = Image::model ()->findByPk (intval($_GET['id']));
        else $model=new Image;
        
        // uncomment the following code to enable ajax-based validation
        if(isset($_POST['ajax']) && $_POST['ajax']==='image-image-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['Image'])) {
            $model->attributes=$_POST['Image'];
            $model->image=CUploadedFile::getInstance($model,'image');
            if($model->validate()) {
                if  ($model->save()) {
                    $title = ($model->title)?"alt=\"{$model->title}\" title=\"{$model->title}\"":"";
                    if (isset($_POST['_add'])) 
                        $this->redirect ($this->adminPath."/".strtolower(get_class($model))."s");
                    else {
                        $image = "<img src=\"{$model->path}\" {$title} onclick=\"insertImage(this);\">";
                        echo json_encode (array("status" => "success", "image" => $image));
                        Yii::app()->end();
                    }
                }
            }
            else if (!isset($_POST['_add'])) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }
        
        $this->render('image',array('model'=>$model));
    }
    
    public function actionResource() {
        if (isset($_GET['id'])) $model = Resource::model ()->findByPk (intval($_GET['id']));
        else $model=new Resource;
        
        $is_new = true;

        $id_language = isset($_GET['id_language'])?intval($_GET['id_language']):false;
        $id_variable = isset($_GET['id_variable'])?intval($_GET['id_variable']):false;
        if ($id_language && $id_variable) {
            $is_new = false;
            $model->id_language = $id_language;
            $model->id_variable = $id_variable;
        }
        
        
        if(isset($_POST['ajax']) && $_POST['ajax']==='resource-resource-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['Resource'])) {
            $model->attributes=$_POST['Resource'];
            if($model->validate()) {
                if ($model->save()) 
                    $this->redirect (Yii::app()->params['adminPath'].'/languages');
            }
        }
        $this->render('resource',array('model'=>$model, "is_new" => $is_new));
    }
    
    public function actionResourcevar() {
        if (isset($_GET['id'])) $model = Resourcevar::model ()->findByPk (intval($_GET['id']));
        else $model=new Resourcevar;
        // uncomment the following code to enable ajax-based validation
        if(isset($_POST['ajax']) && $_POST['ajax']==='resource-var-resourcevar-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_POST['Resourcevar'])) {
            $model->attributes=$_POST['Resourcevar'];
            if($model->validate()) {
                if ($model->save()) 
                    $this->redirect (Yii::app()->params['adminPath'].'/languages');
            }
        }
        $this->render('resourcevar',array('model'=>$model));
    }
    
    public function actionComments() {
        $model = new Comment('search');
        if(isset($_GET['Comment']))
            $model->attributes=$_GET['Comment'];
        if (isset($_GET['id_language']))
            $model->id_language = intval($_GET['id_language']);
        $this->render('viewComments', array("model" => $model));
    }
    
    public function actionApproveComment() {
        $id = intval(@$_GET['id']);
        Comment::model()->updateByPk($id, array("is_approved" => 1));
        $this->redirect($this->adminPath."/comments");
    }
    
    public function actionComment() {
        if (isset($_GET['id'])) $model = Comment::model ()->findByPk (intval($_GET['id']));
        else $model=new Comment;

        // uncomment the following code to enable ajax-based validation
        if(isset($_POST['ajax']) && $_POST['ajax']==='comment-comment-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_GET['id_language'])) {
            $language = intval($_GET['id_language']);
            $model->id_language = $language;
        }
        
        if(isset($_POST['Comment'])) {
            $model->attributes=$_POST['Comment'];
            $model->create_date = $this->_now;
            if (empty($model->is_approved)) 
                $model->is_approved = 0;
            if($model->validate()) {
                $model->save();
                $this->redirect($this->adminPath."/comments".(isset($_POST['lid'])?"/".intval($_POST['lid']):""));
            }
        }
        
        $language = isset($_GET['id_language'])?Language::model()->findByPk(intval($_GET['id_language'])): false;
        $this->render("comment", array("model" => $model, "language" => $language));
    }
}
