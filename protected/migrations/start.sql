--
-- Структура таблицы `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `salt` varchar(32) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `permission` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `salt`, `first_name`, `last_name`, `email`, `create_date`, `permission`) VALUES
(1, 'Admin', '6f8ZMDoXI9TIU', '8d1602ed5a1e033d0aaf01da47b56576', 'Главный', 'Администратор', '', '2014-10-30 17:25:36', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_admins_session`
--

CREATE TABLE IF NOT EXISTS `tbl_admins_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_admin` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(72) NOT NULL,
  `user_agent` varchar(100) DEFAULT NULL,
  `open_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `is_closed` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_article`
--

CREATE TABLE IF NOT EXISTS `tbl_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_autor` int(11) DEFAULT NULL,
  `path` varchar(100) NOT NULL,
  `script` varchar(100) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `tbl_article`
--

INSERT INTO `tbl_article` (`id`, `id_autor`, `path`, `script`, `comment`, `create_date`, `update_date`) VALUES
(1, 1, 'index', NULL, 'Стартовая страница', '2014-10-10 17:59:27', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_images`
--

CREATE TABLE IF NOT EXISTS `tbl_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_article` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `width` int(5) NOT NULL DEFAULT '1',
  `height` int(5) NOT NULL DEFAULT '1',
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`id_article`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_language`
--

CREATE TABLE IF NOT EXISTS `tbl_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(3) DEFAULT NULL,
  `icon` text,
  `is_default` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tbl_language`
--

INSERT INTO `tbl_language` (`id`, `name`, `type`, `icon`, `is_default`) VALUES
(1, 'Русский', 'ru', '/language/ru.png', 1),
(2, 'English', 'en', '/language/en.png', 0),
(3, 'Deutsch', 'de', '/language/de.png', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) NOT NULL,
  `id_article` int(11) DEFAULT NULL,
  `anchor` varchar(100) DEFAULT NULL,
  `custom_path` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `id_owner` int(4) DEFAULT NULL,
  `position` int(4) DEFAULT NULL,
  `is_new` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_menu`
--

INSERT INTO `tbl_menu` (`id`, `id_language`, `id_article`, `anchor`, `custom_path`, `title`, `id_owner`, `position`, `is_new`) VALUES
(1, 1, 1, '', '', 'Главная', NULL, 1, 0),
(2, 1, NULL, '', 'ed.panel', 'Админ-панель', NULL, 2, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_page`
--

CREATE TABLE IF NOT EXISTS `tbl_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_article` int(11) DEFAULT NULL,
  `id_language` int(11) DEFAULT NULL,
  `title` varchar(512) DEFAULT NULL,
  `content` text,
  `keywords` varchar(512) DEFAULT NULL,
  `doc_file` varchar(100) DEFAULT NULL,
  `is_show` int(1) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `tbl_page`
--

INSERT INTO `tbl_page` (`id`, `id_article`, `id_language`, `title`, `content`, `keywords`, `doc_file`, `is_show`, `create_date`, `update_date`) VALUES
(1, 1, 1, 'Добро пожаловать', '<p>Мы рады приветствовать Вас на страницах нашего сайта.</p>\r\n<p>Для редактирования этой страницы и входа в админ-панель воспользуетесь <a href="/ed.panel" target="_blank">этой ссылкой</a></p>\r\n<p>Логин: Admin</p>\r\n<p>Пароль: 111111</p>', 'Kitary СMS', NULL, 1, '2014-10-30 16:53:58', '2014-10-30 16:53:58');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_resources`
--

CREATE TABLE IF NOT EXISTS `tbl_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_resources`
--

INSERT INTO `tbl_resources` (`id`, `id_language`, `name`, `value`) VALUES
(1, 1, 'Description', 'Kitary CMS');