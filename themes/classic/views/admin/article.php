<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
        Yii::t('main', 'Articles') => Yii::app()->params['adminPath']."/articles",
        Yii::t('article', 'n#Add new article|!n#View {path} article', array($model->isNewRecord, "{path}" => $model->path)),
);
?>

<h1><?= Yii::t('article', 'n#Add new article|!n#View \'{path}\' article', array($model->isNewRecord, "{path}" => $model->path)) ?></h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'article-article-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note"><?= Yii::t('main', 'Fields with') ?> <span class="required">*</span> <?= Yii::t('main', 'are required') ?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'path'); ?>
		<?php echo $form->textField($model,'path'); ?>
		<?php echo $form->error($model,'path'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textField($model,'comment'); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>
        
        <?php if (!$model->isNewRecord && 0) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'id_autor'); ?>
		<?php echo $form->textField($model,'id_autor'); ?>
		<?php echo $form->error($model,'id_autor'); ?>
	</div>
        <?php } ?>
<!--
	<div class="row">
            
            <?php echo $form->labelEx($model,'script'); ?>
            <span class="_hint">Регистрация прошла успешно</span>
            <?php echo $form->textField($model,'script'); ?>
            <?php echo $form->error($model,'script'); ?>
	</div>
-->
        <?php if (!$model->isNewRecord) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'create_date'); ?>
		<?php echo $form->textField($model,'create_date', array("disabled" => "disabled")); ?>
		<?php echo $form->error($model,'create_date'); ?>
	</div>
        <?php } ?>

        <?php if ($model->update_date) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'update_date'); ?>
		<?php echo $form->textField($model,'update_date', array("disabled" => "disabled")); ?>
		<?php echo $form->error($model,'update_date'); ?>
	</div>
        <?php } ?>

	<div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('main', 'n#Create|!n#Save', array($model->isNewRecord))); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->