<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
        Yii::t('main', 'Languages') => Yii::app()->params['adminPath']."/languages",
        Yii::t('language', 'n#Add new language|!n#View {language} language', array($model->isNewRecord, "{language}" => $model->name))
);
?>
<h1><?= Yii::t('language', 'n#Add new language|!n#View \'{language}\' language', array($model->isNewRecord, "{language}" => $model->name)) ?></h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'language-language-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>true,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note"><?= Yii::t('main', 'Fields with') ?> <span class="required">*</span> <?= Yii::t('main', 'are required') ?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->textField($model,'type'); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

        <?php if ($model->icon) { ?>
	<div class="row">
            <?php echo CHtml::image($model->icon, "", array("style" => "width: 50px; height: 32px;")); ?>
	</div>
        <?php } ?>
        
        <div class="row">
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo $form->fileField($model,'image'); ?>
	</div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'is_default'); ?>
            <?php 
            echo $form->dropDownList($model, "is_default",
                array(0 => Yii::t('main', "no"), 1 => Yii::t('main', "yes")),
                array("style" => "width: 70px;")
            );
            ?>
            <?php echo $form->error($model,'is_default'); ?>
        </div>


	<div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('main', 'n#Add|!n#Save', array($model->isNewRecord))); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->