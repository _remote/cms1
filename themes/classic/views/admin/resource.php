<?php
$models = Resourcevar::model()->findAll(array('order' => 'id'));
$vlist = CHtml::listData($models, 'id', 'name');

$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
    Yii::t('main', 'Languages') => Yii::app()->params['adminPath']."/languages",
    Yii::t('resource', 'n#Set value of variable|!n#Change value of variable', array($model->isNewRecord, "{variable}" => $vlist[$model->id_variable])),
);
?>

<h1><?= Yii::t('resource', 'n#Set value of variable \'{variable}\'|!n#Change value of \'{variable}\'', array($model->isNewRecord, "{variable}" => $vlist[$model->id_variable])) ?></h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'resource-resource-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?= Yii::t('main', 'Fields with') ?> <span class="required">*</span> <?= Yii::t('main', 'are required') ?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
            <?php
            $models = Language::model()->findAll(array('order' => 'id'));
            $list = CHtml::listData($models, 'id', 'name');
            ?>
            <?php echo $form->labelEx($model,Yii::t('resource', 'Id Language')); ?>
            <?php 
            if ($model->isNewRecord && $is_new)
                echo $form->dropDownList($model, 'id_language', $list, array("style" => "width: 200px;")); 
            else echo $form->textField($model,'id_language', array("value" => $list[$model->id_language], "disabled" => "disabled", "style" => "width: 200px;"));
            ?>
            <?php echo $form->error($model,'id_language'); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,Yii::t('resource', 'Id Variable')); ?>
            <?php 
            if ($model->isNewRecord  && $is_new)
                echo $form->dropDownList($model, 'id_variable', $vlist, array("style" => "width: 200px;")); 
            else echo $form->textField($model,'id_variable', array("value" => $vlist[$model->id_variable], "disabled" => "disabled", "style" => "width: 200px;"));
            ?>
            <?php echo $form->error($model,'id_variable'); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'value'); ?>
            <?php echo $form->textField($model,'value', array("style" => "width: 700px;")); ?>
            <?php echo $form->error($model,'value'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('main', 'n#Add|!n#Save', array($model->isNewRecord))); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->