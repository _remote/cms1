<?php
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
    @date_default_timezone_set(@date_default_timezone_get());

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
$db = require(dirname(__FILE__).'/db.php');
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'_Console',
    
        'commandMap'=>array(
            'migrate'=>array(
                'class'=>'system.cli.commands.MigrateCommand',
                'migrationPath'=>'application.migrations',
                'migrationTable'=>'b1_migration',
                'connectionID'=>'db',
                'interactive' => false,
                //'templateFile'=>'application.migrations.template',
            ),
        ),
    
        'components'=>  array_merge($db, array(
            
        )),
);