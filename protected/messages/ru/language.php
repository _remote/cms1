<?php
return array(
    "n#Add new language|!n#View {language} language" => "n#Добавить язык|!n#Редактирование языка {language}",
    "n#Add new language|!n#View '{language}' language" => "n#Добавление нового языка|!n#Редактирование языка '{language}'",
    "View languages" => "Локализация сайта",
    "View site languages" => "Таблица языков сайта",
    "Add language" => "Добавить язык",
    "Please choose icon file." => "Укажите изображение для языка",
    "Name" => "Название",
    "Type" => "Тип",
    "Icon" => "Флаг",
    "Is Default" => "По умолчанию",
    "Choose icon" => "Выберите флаг",
    "Count pages" => "Всего страниц",
    "Count menus item" => "Всего элементов меню",
    "" => "",
    "" => "",
);

