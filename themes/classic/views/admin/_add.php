<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
    Yii::t('admin', 'n#Add new admin|!n#Edit {username}', array($model->isNewRecord, "{username}" => $model->username)),
);
?>

<h1><?= Yii::t('admin', 'n#Add new admin|!n#Edit {username}', array($model->isNewRecord, "{username}" => $model->username)) ?></h1>
<div class="form">

<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'admin-_add-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>true,
)); 
$disabled = (!$model->isNewRecord)?array("disabled" => "disabled"):array();
?>

	<p class="note"><?= Yii::t('main', 'Fields with') ?> <span class="required">*</span> <?= Yii::t('main', 'are required') ?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username', $disabled); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
        
        <?php if ($model->isNewRecord) { ?>
        <div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->textField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
        <?php } ?>
        
        <div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name'); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name'); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div>

	
        <?php if (!$model->isNewRecord) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'create_date'); ?>
		<?php echo $form->textField($model,'create_date', $disabled); ?>
		<?php echo $form->error($model,'create_date'); ?>
	</div>
        <?php } ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('main', 'n#Add|!n#Save', array($model->isNewRecord))); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->