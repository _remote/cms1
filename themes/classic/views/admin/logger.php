<?php
$this->pageTitle=Yii::app()->name . ' - Log actions';
$this->breadcrumbs=array("Admin" => "index", 
	'Log actions',
);

$type_list = array("0" => "All", "action" => "Actions", "payment" => "Payments", "default" => "Messages", "error" => "Errors", "none" => "Empty");

function serchColor($str = "", $serch = "") {
    $position = strpos(strtolower($str), strtolower($serch));
    $length = strlen($serch);
    $find = substr($str, $position, $length);
    $result = preg_replace("/{$serch}/i", "<b style=\"color: #366097;\">{$find}</b>", $str);
    return $result;
}
?>

<h1>Log information</h1>

<div class="search">
    <form action="/admin/logger" method="post">
        <label>Login</label>
        <input type="text" name="login" value="<?= $params['login'] ?>">
        <label>Type</label>
        <select name="type">
            <?php
            foreach ($type_list as $key=>$t) {
                $active = (isset($params['type']) && $params['type'] == (string)$key)?"selected=\"selected\"":"";
                echo "<option value=\"{$key}\" {$active}>{$t}</option>";
            }
            ?>
        </select>
        <input type="submit" name="action" value="Search" style="margin-left: 30px;">
        <input type="submit" name="action" value="Clear">
    </form>
</div>

<?php
echo "<table class=\"use_table\">
    <tr>
        <th>Id</th>
        <th>Login</th>
        <th>Text</th>
        <th>Comment</th>
        <th>Type</th>
        <th>Time</th>
    </tr>";
if ($params['logrows']) {
    foreach ($params['logrows'] as $log) {      
                
        if ($params['login']) 
            $user_login = serchColor($log->user_login, $params['login']);
        else $user_login = $log->user_login;
        
        echo "<tr class=\"row\">
                <td>{$log->id}</td>
                <td>{$user_login}</td>
                <td>{$log->text}</td>
                <td>{$log->comment}</td>
                <td>{$type_list[$log->type]}</td>
                <td>{$log->date}</td>
              </tr>";
    }
}
echo "</table>";

if (!$params['serch'])
    $this->renderPartial('_pager',array("params" => array("link" => "/admin/logger/", "page" => $params['page'], "count_pages" => $params['count_pages'])));
?>
