<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => $this->adminPath,
    Yii::t('article', 'View articles'),
);
?>

<h1><?= Yii::t('article', 'View site articles') ?></h1>
<?php
$columns = array(
    array(
        'name'=>'id',
        'filter' => false,
    ),
    'path' => array(
        'name' => 'path',
        'type'=>'html',
        'value' => 'CHtml::link(CHtml::encode($data->path), 
             array("'.$this->adminPath.'/article/$data->id"))',
    ),
    'comment',
    array(
        'name'=>'create_date',
        'filter' => false,
    ),
    array(
        'name'=>'update_date',
        'filter' => false,
    ),
    array(
        'class'=>'CButtonColumn',
        'template'=>'{update} {delete}',
        'buttons'=>array
        (
            'update' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/article/$data->id")',
            ),
            'delete' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/delete/article/$data->id")',
            ),
        ),
    ),
);

$this->widget('zii.widgets.grid.CGridView', array(
        'enablePagination' => true,
        'dataProvider'=>$model->search(),
	'filter'=>$model,
        'columns'=>$columns,
));
?>

<div class="_button_row">
    <div class="_button" onclick="location='<?= $this->adminPath ?>/article'"><?= Yii::t('article', 'Add article') ?></div>
</div>

