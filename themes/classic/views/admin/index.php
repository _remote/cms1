<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
        Yii::t('admin', 'My profile')
);
?>

<h1><?= Yii::t('admin', 'Profile detail view') ?></h1>
<?php
$attributes = array(
    'username',
    'email',
    'first_name',
    'last_name',
    'create_date',
);

$this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>$attributes,
));
?>

<div class="_button_row">
    <div class="_button" onclick="location='<?= $this->adminPath ?>/password'"><?= Yii::t('admin', 'Change password') ?></div>
</div>

<h1><?= Yii::t('admin', '{username} session history', array("{username}" => @$model->username)) ?></h1>
<?php
$columns = array(
    'id',
    'open_date',
    'update_date',
    'user_agent',
    'ip',
    'is_closed',
);

$this->widget('zii.widgets.grid.CGridView', array(
        'enablePagination' => true,
        'dataProvider'=> new CActiveDataProvider($session, array('sort'=>array('defaultOrder'=>'open_date DESC, update_date DESC'))),
        //'filter'=> $session,
        'columns'=>$columns,
));
?>

<h1><?= Yii::t('admin', 'Admins list') ?></h1>
<?php
$columns = array(
    array(
        'name' => 'id',
        'filter' => false,
    ),
    array(
        'name' => 'username',
    ),
    array(
        'name' => 'email',
    ),
    array(
        'name' => 'first_name',
    ),
    array(
        'name' => 'last_name',
    ),
    array(
        'name' => 'create_date',
        'filter' => false,
    ),
    array(
        'class'=>'CButtonColumn',
        'template'=>'{update} {delete}',
        'buttons'=>array
        (
            'update' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/".strtolower(get_class($data))."/$data->id")',
            ),
            'delete' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/delete/".strtolower(get_class($data))."/$data->id")',
            ),
        ),
    ),
);

$this->widget('zii.widgets.grid.CGridView', array(
        'enablePagination' => true,
        'dataProvider'=> $admins->search(),
        'filter'=> $admins,
        'columns'=>$columns,
));
?>

<div class="_button_row">
    <div class="_button" onclick="location='<?= $this->adminPath ?>/add'"><?= Yii::t('admin', 'Add admin') ?></div>
</div>