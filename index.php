<?php
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

defined('YII_LOCAL') or define('YII_LOCAL',true);

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following line when in production mode
//defined('YII_DEBUG') or define('YII_DEBUG',true);

require_once($yii);
Yii::createWebApplication($config)->run();
