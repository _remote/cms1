<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
        Yii::t('main', 'Comments') => Yii::app()->params['adminPath']."/comments",
);
if ($language)
    $this->breadcrumbs = array_merge($this->breadcrumbs, array("{$language->type}" => Yii::app()->params['adminPath']."/comments/".$language->id));
array_push($this->breadcrumbs, (Yii::t('comment', 'n#Add new comment|!n#View comment from {name}', array($model->isNewRecord, "{name}" => $model->name))));
?>
<h1><?= Yii::t('comment', 'n#Add new comment|!n#View comment', array($model->isNewRecord)) ?></h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comment-comment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note"><?= Yii::t('main', 'Fields with') ?> <span class="required">*</span> <?= Yii::t('main', 'are required') ?>.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php
        if ($language) 
            echo Chtml::hiddenField('lid', $language->id);
        ?>
        
        <div class="row">
            <?php
            $models = Language::model()->findAll(array('order' => 'id'));
            $list = CHtml::listData($models, 'id', 'name');
            ?>
            <?php echo $form->labelEx($model,'id_language'); ?>
            <?php echo $form->dropDownList($model, 'id_language', $list, array("style" => "width: 200px;")); ?>
            <?php echo $form->error($model,'id_language'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text', array("style" => "width: 500px; height: 300px;")); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_approved'); ?>
		<?php echo $form->dropDownList($model, 'is_approved', array("0" => Yii::t('main', "no"), "1" => Yii::t('main', "yes")), array("style" => "width: 70px;")); ?>
		<?php echo $form->error($model,'is_approved'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

        <?php if (!$model->isNewRecord) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'create_date'); ?>
		<?php echo $form->textField($model,'create_date', array("disabled" => "disabled")); ?>
		<?php echo $form->error($model,'create_date'); ?>
	</div>
        <?php } ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('main', 'n#Create|!n#Save', array($model->isNewRecord))); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->