<?php

// this contains the application parameters that can be maintained via GUI
return array(
	// this is displayed in the header section
	'title'=>'Центр тибетской медицины "Ваджра Гаруда"',
	// this is used in error pages
	'adminEmail'=>'admin@garuda.com.ua',
	// the copyright information displayed in the footer section
	'copyrightInfo'=>'&copy; '.date('Y').' <a href="mailto:info@garuda.com.ua">Ваджра Гаруда</a>. ',
    
        'adminPath' => '/ed.panel',
    
        'logoFile'  => '/images/emblem.jpg',
    
        'pageSize'  => 20,
    
        'contacts' => array(
            "skype" => "garuda11111",
            "email" => "botsula@list.ru",
            "telephone" => "+38 (067) 750-57-63",
            "vk_gp_id" => "",
        ),
);
