<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
        Yii::t('main', 'Site menu') => Yii::app()->params['adminPath']."/menus",
);
if ($language)
    $this->breadcrumbs = array_merge($this->breadcrumbs, array("{$language->type}" => Yii::app()->params['adminPath']."/menus/".$language->id));
array_push($this->breadcrumbs, (Yii::t('menu', 'n#Add new menu item|!n#View {name} menu item', array($model->isNewRecord, "{name}" => $model->title))));
?>
<h1><?= Yii::t('menu', 'n#Add new menu item|!n#View \'{name}\' menu item', array($model->isNewRecord, "{name}" => $model->title)) ?></h1>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'menu-menu-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note"><?= Yii::t('main', 'Fields with') ?> <span class="required">*</span> <?= Yii::t('main', 'are required') ?>.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php
        if ($language) 
            echo Chtml::hiddenField('lid', $language->id);
        ?>

	<div class="row">
                <?php
                $models = Language::model()->findAll(array('order' => 'id'));
                $list = CHtml::listData($models, 'id', 'name');
                ?>
		<?php echo $form->labelEx($model,Yii::t('menu', 'Language')); ?>
		<?php echo $form->dropDownList($model, 'id_language', $list, array("style" => "width: 200px;")); ?>
		<?php echo $form->error($model,'id_language'); ?>
	</div>

	<div class="row">
            <?php $list = Article::model()->getList(); ?>
            <?php echo $form->labelEx($model,Yii::t('menu', 'Article')); ?>
            <?php echo $form->dropDownList($model, 'id_article', $list, array("style" => "width: 300px;", "empty" => Yii::t('menu', "Without article"))); ?>
            <?php echo "#".$form->textField($model,'anchor', array("style" => "margin-left: 7px; width: 100px;", "title" => Yii::t('main', "Link anchor"))); ?>
            <?php echo $form->error($model,'id_article'); ?>
            <br><?= Yii::t('main', "or") ?><br>
            <?php echo $form->labelEx($model,'custom_path', array("style" => "margin-top: 5px;")); ?>
            <?php echo $form->textField($model,'custom_path'); ?>
            <?php echo "#".$form->textField($model,'anchor', array("style" => "margin-left: 7px; width: 100px;", "title" => Yii::t('main', "Link anchor"))); ?>
	</div>
        
        <div class="row">
            <?php
            $models = Menu::model()->findAll(array('order' => 'id'));
            $list = CHtml::listData($models, 'id', 'title');
            ?>
            <?php echo $form->labelEx($model,'id_owner'); ?>
            <?php echo $form->dropDownList($model, 'id_owner', $list, array("style" => "width: 200px;", "empty" => Yii::t('menu', "It`s owner item"))); ?>
            <?php echo $form->error($model,'id_owner'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title'); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'is_new'); ?>
            <?php //echo $form->checkBox($model,'is_new', array("style" => "float: none;")); ?>
            <?php 
            echo $form->dropDownList($model, "is_new",
                array(0 => Yii::t('main', "no"), 1 => Yii::t('main', "yes")),
                array("style" => "width: 70px;")
            );
            ?>
            <?php echo $form->error($model,'is_new'); ?>
	</div>
        
        
	
        
        <?php if (!$model->isNewRecord) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php echo $form->textField($model,'position', array("disabled" => "disabled")); ?>
		<?php echo $form->error($model,'position'); ?>
	</div>
        <?php } ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('main', 'n#Create|!n#Save', array($model->isNewRecord))); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->