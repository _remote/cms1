<?php

/**
 * This is the model class for table "{{menu}}".
 *
 * The followings are the available columns in table '{{menu}}':
 * @property integer $id
 * @property integer $id_language
 * @property integer $id_article
 * @property string  $anchor
 * @property string  $custom_path
 * @property string  $title
 * @property integer $id_owner
 * @property integer $position
 * @property integer $is_new
 */
class Menu extends CActiveRecord
{
    
        public $article_search;
        public $language_search;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{menu}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('id_language, is_new', 'required'),
			array('id_language, id_article, id_owner, position, is_new', 'numerical', 'integerOnly'=>true),
			array('title, anchor, custom_path', 'length', 'max'=>100),
                        array('id_article','uniqueLanguageWithArticle', 'message' => Yii::t('menu', 'For this language already has such article')),
                        array('id_owner','forEmptyArticleOrPath', 'message' => Yii::t('menu', 'Empty article can be set only for chield menu items. Please set owner item.')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_language, id_article, anchor, custom_path, title, id_owner, position, is_new, article_search, language_search', 'safe', 'on'=>'search'),
		);
	}
        
        public function uniqueLanguageWithArticle($attribute,$params=array()) {
            if(!$this->hasErrors()) {
                $params['criteria']=array(
                    'condition'=>'id_language=:id_language',
                    'params'=>array(':id_language'=>$this->id_language),
                );
                
                $validator=CValidator::createValidator("unique",$this,$attribute,$params);
                $validator->validate($this,array($attribute));
            }
        } 
        
        public function forEmptyArticleOrPath($attribute,$params=array()) {
            if(!$this->hasErrors() && empty($this->id_article) && empty($this->custom_path)) {
                $validator=CValidator::createValidator("required",$this, $attribute, $params);
                $validator->validate($this,array($attribute));
            }
        } 

	/**
	 * @return array relational rules.
	 */
	public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'article' => array(self::BELONGS_TO, 'Article', 'id_article'),
                'language' => array(self::BELONGS_TO, 'Language', 'id_language'),
            );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
                    'id' => Yii::t('menu', 'ID'),
                    'id_language' => Yii::t('menu', 'Id Language'),
                    'id_article' => Yii::t('menu', 'Id Article'),
                    'anchor' => Yii::t('menu', 'Anchor'),
                    'custom_path' => Yii::t('menu', 'Custom path'),
                    'title' => Yii::t('menu', 'Title'),
                    'id_owner' => Yii::t('menu', 'Owner item'),
                    'position' => Yii::t('menu', 'Position'),
                    'is_new' => Yii::t('menu', 'Is New'),
                    'article_search' => Yii::t('menu', 'Link'),
                    'language_search' => Yii::t('menu', 'Language'),
		);
	}
        
        public function loadItems($language = false, $labels_only = false, $link_with_lang = true, $id_owner = false) {
            $menu = array();
            if ($language) {
                $criteria = new CDbCriteria();
                $criteria->with = array("language" => array("select" => array("id", "type")), "article" => array("select" => array("path")));
                //$criteria->select = 't.`title`';
                $criteria->condition = "language.`type` = :language AND id_owner ".(($id_owner)?" = :id_owner":"IS NULL");
                $criteria->params = array(':language' => $language);
                if ($id_owner)
                    $criteria->params = array_merge ($criteria->params, array(':id_owner' => $id_owner));
                $criteria->order = "position ASC";
                $_menu=Menu::model()->findAll($criteria);
                if ($_menu) {
                    foreach ($_menu as $item) {
                        $i = array();
                        $count = $item->countItems();
                        $i['label'] = $item['title'].(($count && $labels_only)?" ({$count}) ▾":"");
                        $path = (!empty($item['article']['path']))?$item['article']['path']:$item['custom_path'];
                        if (!$labels_only) $i['url'] = array(($link_with_lang && !empty($item['article']['path']))?"/{$item['language']['type']}/{$path}":"/{$path}");
                        else $i['itemOptions'] = array("rel" => $item['id'], "lang" => $item['language']['type'], 'onclick' => "loadSubItems(this);");
                        $menu[] = $i;
                    }
                }
            }
            return $menu;
        }
        
        public function loadFullMenu($language = "ru", $link_with_lang = true) {
            $menu = array();
            $criteria = new CDbCriteria();
            $criteria->with = array("language" => array("select" => array("id", "type")), "article" => array("select" => array("path")));
            //$criteria->select = 't.`title`';
            $criteria->condition = "language.`type` = :language";
            $criteria->params = array(':language' => $language);
            $criteria->order = "id_owner ASC, position ASC";
            $_menu=Menu::model()->findAll($criteria);
            if ($_menu) {
                //Формируем полный список всего меню для выбранного языка
                $temp_menu = array();
                foreach ($_menu as $item) {
                    $i = array();
                    if (!empty($item['title']))
                        $i['label'] = $item['title'];
                    $i['id_owner'] = $item['id_owner'];
                    $i['position'] = $item['position'];
                    $i['is_new'] = $item['is_new'];
                    if (!empty($item['article']['path']) || !empty($item['custom_path'])) {
                        $path = (!empty($item['id_article']))?$item['article']['path']:$item['custom_path'];
                        $i['url'] = ($link_with_lang && !empty($item['id_article']))?"/{$item['language']['type']}/{$path}":"/{$path}";
                        if (!empty($item['anchor'])) $i['url'] .= "#".$item['anchor'];
                    }
                    $temp_menu[$item['id']] = $i;
                }
                //Сортируем список на дочерние и родительские
                $chield = array();
                foreach ($temp_menu as $id => $item) {
                    $item['id'] = $id;
                    if (empty($item['id_owner'])) $menu[$id] = $item;
                    else $chield[$id] = $item;
                }
                //Раскидываем дочерние элементы по родительским
                foreach ($chield as $id => $item) {
                    $this->putChieldItem($menu, $item);
                }
            }
            
            return $menu;
        }
        
        private function putChieldItem(&$menu, $subitem, $level = 1) {
            foreach ($menu as $key => &$item) {
                if (isset($item['id']) && $item['id'] == $subitem['id_owner']) {
                    if (!empty($subitem['label']))
                        $subitem['level'] = $level;
                    else $subitem = "-";
                    
                    if (isset($subitem['position']) && !isset($item['items'][$subitem['position']]))
                        $item['items'][$subitem['position']] = $subitem;
                    else $item['items'][] = $subitem;
                    return true;
                }
                else if (isset($item['items']) && is_array($item['items'])) {
                    if ($this->putChieldItem($item['items'], $subitem, ++$level)) 
                            return true;
                }
                else continue;
            }
            return false;
        }
        
        public function countItems($id = false) {
            $_id = ($id)?$id:$this->id;
            return ($_id)?($this->model()->count("id_owner = :id", array(":id" => $_id))):0;
        }
        
        public function countItemsForLanguage($id_language = false) {
            $_id = ($id_language)?$id_language:$this->id_language;
            return ($_id)?($this->model()->count("id_language = :id_language", array(":id_language" => $_id))):0;
        }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id = false) {
		$criteria=new CDbCriteria;
                $criteria->with = array('article', 'language' );
                
		$criteria->compare('id',$this->id);
		$criteria->compare('id_language',($id)?$id:$this->id_language);
		$criteria->compare('id_article',$this->id_article);
                $criteria->compare('anchor',$this->anchor, true);
                $criteria->compare('custom_path',$this->custom_path);
		$criteria->compare('title',$this->title,true);
                $criteria->compare('id_owner',$this->id_owner);
		$criteria->compare('position',$this->position);
		$criteria->compare('is_new',$this->is_new);
                
                $criteria->compare( 'article.path', $this->article_search, true );
                $criteria->compare( 'language.name', $this->language_search, true );

		return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'sort'=>array(
                        'attributes'=>array(
                            'article_search'=>array(
                                'asc'=>'article.path',
                                'desc'=>'article.path DESC',
                            ),
                            'language_search'=>array(
                                'asc'=>'language.name',
                                'desc'=>'language.name DESC',
                            ),
                            '*',
                        ),
                    ),
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['pageSize'],
                    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Menu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
