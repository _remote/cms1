<?php

class m141121_145746_comments extends CDbMigration {
	public function up() {
            $sql = "CREATE TABLE IF NOT EXISTS `tbl_comment` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `id_language` int(11) DEFAULT NULL,
                        `name` varchar(50) DEFAULT NULL,
                        `email` varchar(50) DEFAULT NULL,
                        `text` text NOT NULL,
                        `create_date` datetime DEFAULT NULL,
                        `is_approved` int(1) DEFAULT NULL,
                        PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
            $this->execute($sql);
	}

	public function down() {
            $this->dropTable('tbl_comment');
	}
}