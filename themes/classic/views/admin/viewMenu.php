<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => $this->adminPath,
    Yii::t('menu', 'Site menu items'),
);
?>

<h1><?= Yii::t('menu', 'View menu items') ?></h1>
<?php
$models = Language::model()->findAll(array('order' => 'id'));
$list = CHtml::listData($models, 'id', 'name');
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'menu-form-filter',
    'method'=>'get',
    'enableAjaxValidation'=>true,
    'action'=>Yii::app()->createUrl($this->adminPath.'/menus'),
));
echo $form->labelEx($model,Yii::t('main', "Filter:"))."<br>";
echo $form->dropDownList($model, 'id_language', $list, array("style" => "width: 170px;", "empty" => Yii::t('main', "Select language filter")));
echo CHtml::submitButton(Yii::t('main', 'Set filter'));  
$this->endWidget();

$columns = array(
    array(
        'name'=>'id',
        'filter' => false,
    ),
    array(
        'name'=>'id_owner',
        'header' => Yii::t('menu', 'Owner ID'),
        'type'=>'raw',
        'value' => 'CHtml::link(CHtml::encode($data->id_owner), '
        . 'array("'.$this->adminPath.'/menu/".$data->id_owner))',
        //'filter' => false,
    ),
    
    array(
        'name' => 'article_search',
        'htmlOptions' => array('style' => 'width: 100px;'),
        'type'=>'html',
        'value' => '(isset($data->article->id))?CHtml::link(CHtml::encode($data->article->path), '
        . 'array("'.$this->adminPath.'/article/".$data->article->id)):"null"',
    ),
    array(
        'name'=>'custom_path',
        'htmlOptions' => array('style' => 'width: 100px;'),
    ),
    array(
        'name'=>'anchor',
        'htmlOptions' => array('style' => 'width: 70px;'),
    ),
    array(
        'name'=>'title',
        'htmlOptions' => array('style' => 'width: 300px;'),
    ),
    array(
        'name'=>'position',
        'htmlOptions' => array('style' => 'width: 70px;'),
        'filter' => false,
    ),
    array(
        'name' => 'language_search',
        'type'=>'html',
        'value' => 'CHtml::link(CHtml::encode($data->language->name), '
        . 'array("'.$this->adminPath.'/language/".$data->language->id))',
        'filter' => false,
    ),
    array(
        'name'=>'is_new',
        'header' => Yii::t('menu', 'New'),
        'type'=>'raw',
        'value' => '!empty($data->is_new)?"<span style=\"color: green;\">'.Yii::t('main', "yes").'</span>":"'.Yii::t('main', "no").'"',
        'filter' => false,
    ),
    array(
        'class'=>'CButtonColumn',
        'template'=>'{update} {delete}',
        'buttons'=>array
        (
            'update' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/".strtolower(get_class($data))."/$data->id/'.$model->id_language.'")',
            ),
            'delete' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/delete/".strtolower(get_class($data))."/$data->id")',
            ),
        ),
    ),
);

$this->widget('zii.widgets.grid.CGridView', array(
    'enablePagination' => true,
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>$columns,
    'pager'=>array(
        'pageSize' => 20,
    ),
));

?>

<div class="_button_row">
    <div class="_button" onclick="location='<?= $this->adminPath ?>/menu?id_language=<?= $model->id_language ?>'"><?= Yii::t('menu', 'Add menu item') ?></div>
    <?php if (!empty($lid)) { ?>
    <div class="_button" onclick="location='<?= $this->adminPath ?>/sortmenu/<?= $lid ?>'"><?= Yii::t('menu', 'Sort menu') ?></div>
    <?php } ?>
</div>

