<?php

/**
 * This is the model class for table "{{images}}".
 *
 * The followings are the available columns in table '{{images}}':
 * @property integer $id
 * @property integer $id_article
 * @property string $name
 * @property string $path
 * @property string $title
 * @property integer $width
 * @property integer $height
 * @property string $create_date
 */
class Image extends CActiveRecord {
    
     public $image;
     public $article_search;
     
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{images}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('id_article, name', 'required'),
                array('id_article, width, height', 'numerical', 'integerOnly'=>true),
                array('name', 'length', 'max'=>50),
                array('path, title', 'length', 'max'=>255),
                array('create_date', 'safe'),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, id_article, name, path, title, width, height, create_date, article_search', 'safe', 'on'=>'search'),
                array('image', 'file', 'types'=>'jpg, gif, png', 'message' => Yii::t('image', 'Please choose photo.'), 'allowEmpty'=>true),
                array('name', 'uniqueNameForArticle', 'message' => Yii::t('image', 'Not uniue photo name for this page')),
                array('image','emptyImageSource', 'message' => Yii::t('image', 'Please choose photo.')),
            );
	}
        
        public function uniqueNameForArticle($attribute,$params=array()) {
            if(!$this->hasErrors()) {
                $params['criteria']=array(
                    'condition'=>'id_article=:id_article',
                    'params'=>array(':id_article'=>$this->id_article),
                );
                
                $validator=CValidator::createValidator("unique",$this,$attribute,$params);
                $validator->validate($this,array($attribute));
            }
        }
        
        public function emptyImageSource($attribute,$params=array()) {
            if(!$this->hasErrors()) {
                $validator=CValidator::createValidator("required",$this, $attribute, $params);
                $validator->validate($this,array($attribute));
            }
        } 
        
	/**
	 * @return array relational rules.
	 */
	public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'article' => array(self::BELONGS_TO, 'Article', 'id_article'),
            );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
            return array(
                'id' => Yii::t('image', 'ID'),
                'id_article' => Yii::t('image', 'Id Article'),
                'name' => Yii::t('image', 'File name'),
                'path' => Yii::t('image', 'Path to image'),
                'title' => Yii::t('image', 'Title'),
                'width' => Yii::t('image', 'Width'),
                'height' => Yii::t('image', 'Height'),
                'create_date' => Yii::t('image', 'Create Date'),
                'image' => Yii::t('image', 'Choose photo'),
                'article_search' => Yii::t('image', 'Article path'),
            );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('article');

		$criteria->compare('id',$this->id);
		$criteria->compare('id_article',$this->id_article);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('width',$this->width);
		$criteria->compare('height',$this->height);
		$criteria->compare('create_date',$this->create_date,true);
                
                $criteria->compare( 'article.path', $this->article_search, true );

		return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'sort'=>array(
                        'attributes'=>array(
                            'article_search'=>array(
                                'asc'=>'article.path',
                                'desc'=>'article.path DESC',
                            ),
                            '*',
                        ),
                    ),
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['pageSize'],
                    ),
		));
	}
        
        protected function beforeSave() {
            $this->create_date = @date("Y-m-d G:i:s", time());
            
            $sourcePath = pathinfo($this->image->getName());
            //Array ( [dirname] => . [basename] => back.jpg [extension] => jpg [filename] => back )
            $tempFile = 'upload/i_'.$sourcePath['basename'];
            $this->image->saveAs($tempFile);
            $thumb = PhpThumbFactory::create($tempFile);
            //$thumb->adaptiveResize(50, 32);  
            $path = 'upload/photo/'.$this->name.'.png';
            $thumb->save($path, 'png');
            $this->path = '/photo/'.$this->name.'.png';
            $this->image = NULL;
            @unlink($tempFile);
            if(!parent::beforeSave())
                return false;
            return true;
        }


        protected function beforeDelete(){
            if(!parent::beforeDelete())
                return false;
            $this->deleteDocument(); // удалили модель? удаляем и файл
            return true;
        }

        public function deleteDocument(){
            $documentPath=Yii::getPathOfAlias('webroot.upload').$this->path;
            if(is_file($documentPath))
                unlink($documentPath);
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Images the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
