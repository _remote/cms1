<?php
$title = Yii::t("video", "Video list");
$this->pageTitle=Yii::app()->name . ' - '.$title;
?>

<h1><?= $title ?></h1>
<div class="body"> 
    <?php

    $this->widget("zii.widgets.CListView", [
        "dataProvider"=>$dataProvider,
        "itemView"=>"_video_list",
        "enablePagination"=>true,
        'ajaxUpdate'=>true,
        'template'=>"{pager}\n{items}",
        //'template'=>"{pager}\n{items}",
    ]);
    ?>
</div>

