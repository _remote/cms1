<?php
class AdminIdentity extends CUserIdentity
{   
    private $_id;
    
    public function authenticate() {
        $record=Admin::model()->findByAttributes(array('username'=>$this->username));
        //echo crypt($this->password, md5($record->salt."_".$record->username)); die();
        if($record===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if($record->password!==crypt($this->password, md5($record->salt."_".$record->username)))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id=$record->id;
            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}
?>
