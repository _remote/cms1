<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
    
    <?php
    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );
    Yii::app()->clientScript->registerCssFile(
        Yii::app()->clientScript->getCoreScriptUrl().
        '/jui/css/base/jquery-ui.css'
    );
    $time = time();
    ?>

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/main.css<?= "?t=".$time ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/form.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/style.css<?= "?t=".$time ?>" />
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.form.js'); ?>
    
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
    <div class="container" id="page">

        <div id="header">
            <div id="logo" style="position: relative;">
                <img src="<?= Yii::app()->params['logoFile']; ?>" width="100px" height="100px" title="<?php echo CHtml::encode(Yii::app()->name); ?>">
                <div class="admin_panel_title"><?= Yii::t('main', 'Editing panel') ?></div>
                <?php if (!Yii::app()->user->isGuest) { ?>
                <div class="system_info">
                    <div class="_info">DBMS: <strong><?= Yii::app()->db->getDriverName() . ' '. Yii::app()->db->getServerVersion(); ?></strong></div>
                    <div class="_info">PHP: <strong><?=phpversion();?></strong></div>
                    <div class="_info">Web-server: <strong><?= $this->sys_apache_version; ?></strong></div>
                    <div class="_info">OS: <strong><?=php_uname('s').' '.php_uname('r');?></strong></div>  
                </div>
                <?php } ?>
            </div>
        </div><!-- header -->

        <div id="mainmenu">
            <?php $this->widget('zii.widgets.CMenu',array(
                'items'=>array(
                    array('label'=>Yii::t('main', 'Admin'), 'url'=>array($this->adminPath.'/index'), 'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('main', 'Pages'), 'url'=>array($this->adminPath.'/pages'), 'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('main', 'Site menu'), 'url'=>array($this->adminPath.'/menus'), 'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('main', 'Articles'), 'url'=>array($this->adminPath.'/articles'), 'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('main', 'Languages'), 'url'=>array($this->adminPath.'/languages'), 'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('main', 'Photos'), 'url'=>array($this->adminPath.'/images'), 'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('main', 'Videos'), 'url'=>array($this->adminPath.'/videos'), 'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('main', 'Comments'), 'url'=>array($this->adminPath.'/comments'), 'visible'=>!Yii::app()->user->isGuest),

                    
                    array('label'=>Yii::t('main', 'Login'), 'url'=>array($this->adminPath.'/login'), 'visible'=>Yii::app()->user->isGuest),
                    array('label'=>Yii::t('main', 'Logout ({username})', array("{username}" => Yii::app()->user->name)), 'url'=>array($this->adminPath.'/logout'), 'visible'=>!Yii::app()->user->isGuest)
                ),
            )); ?>
        </div><!-- mainmenu -->

        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
        )); ?><!-- breadcrumbs -->

        <div class="content">
            <?php echo $content; ?>
        </div>

        <div id="footer">
            <?php echo Yii::app()->params['copyrightInfo']; ?>
            <?= Yii::t("main", "All rights reserved") ?>
        </div><!-- footer -->
    </div><!-- page -->
    
    <div id="adminDialog"></div>
</body>
</html>
