tinymce.init({
    selector: "._TM_area",
    theme: "modern",
    cleanup : true,
    paste_remove_spans: true,
    paste_remove_styles: true,
    cleanup_on_startup : true,
    plugins: [
        "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "table contextmenu directionality emoticons template textcolor paste textcolor"
    ],
    //toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar1: "insertfile undo redo paste pastetext | searchreplace | styleselect | bold italic | fontselect fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview fullscreen code cleanup",
    image_advtab: true,
    //For full URL
    relative_urls: false,
    convert_urls: false,
    remove_script_host : false,
    //extended_valid_elements : "*[*]",
    
    templates: [
        {title: 'Adress', content: '<div class="title">Title</div><p>Adress string</p>'}
    ]
});