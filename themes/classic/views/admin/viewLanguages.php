<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
    Yii::t('language', 'View languages'),
);

$buttons = array(
    'class'=>'CButtonColumn',
    'template'=>'{update} {delete}',
    'buttons'=>array
    (
        'update' => array (
            'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/".strtolower(get_class($data))."/$data->id")',
        ),
        'delete' => array (
            'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/delete/".strtolower(get_class($data))."/$data->id")',
        ),
    ),
);
?>

<h1><?= Yii::t('language', 'View site languages') ?></h1>
<?php
$columns = array(
    'id' => array(
        'name' => 'id',
        'htmlOptions' => array('style' => 'width: 30px; text-align: center;'),
    ),
    'icon' => array(
        'name'=>'icon',
        'type'=>'html',
        'htmlOptions' => array('align' => 'center', 'style' => 'width: 50px;'),
        'value'=>'(!empty($data->icon))?CHtml::image($data->icon."?t='.time().'", "", array("style"=>"width:25px;height:16px;margin: 0px 12px;")):"no image"',

    ),
    'name' => array(
        'name' => 'name',
        'type'=>'html',
        'value' => 'CHtml::link(CHtml::encode($data->name), 
             array("'.Yii::app()->params['adminPath'].'/language/$data->id"))',
    ),
    array(
        'header' => Yii::t('language', 'Count pages'),
        'type'=>'raw',
        'value' => 'CHtml::link(CHtml::encode(Page::model()->countItemsForLanguage($data->id)), 
             array("'.Yii::app()->params['adminPath'].'/pages/$data->id"))',
    ),
    
    array(
        'header' => Yii::t('language', 'Count menus item'),
        'type'=>'raw',
        'value' => 'CHtml::link(CHtml::encode(Menu::model()->countItemsForLanguage($data->id)), 
             array("'.Yii::app()->params['adminPath'].'/menus/$data->id"))',
    ),
    'type',
    'is_default',
    "buttons" => $buttons,
);
$this->widget('zii.widgets.grid.CGridView', array(
    'enablePagination' => true,
    'dataProvider'=> new CActiveDataProvider($model),
    'columns'=>$columns,
));
?>

<div class="_button_row">
    <div class="_button" onclick="location='<?= $this->adminPath ?>/language'"><?= Yii::t('language', 'Add language') ?></div>
</div>

<h1><?= Yii::t('resource', 'View variables') ?></h1>
<?php


$columns = array(
    array(
        "name" => "name",
        "type" =>"html",
        "value" => '$data->name',
    ),
);
$models = Language::model()->findAll(array('order' => 'id'));
$list = CHtml::listData($models, 'id', 'type');


foreach ($list as $id => $name) {
//    Chtml::tag("span", 
//            array("title"=>($res = Resource::model()->find("id_variable = :id_variable AND id_language = :id_language", 
//                    array(":id_variable" => $data->id, ":id_language" => $id)))?$res->value:""));
    $add = 'Chtml::link("+", array("'.Yii::app()->params['adminPath'].'/resource/'.$id.'/$data->id"))';
    $_link = 'Chtml::link(CHtml::image("/images/view.png"), array("'.Yii::app()->params['adminPath'].'/resource/$res->id"))';
    $view = 'Chtml::tag("span", array("title"=>($res = Resource::model()->find("id_variable = :id_variable AND id_language = :id_language", array(":id_variable" => $data->id, ":id_language" => '.$id.')))?$res->value:""), '.$_link.')';
    $item = array(
        'header' => $name,
        'type'=>'raw',
        'value' => '(Resource::model()->count("id_variable = :id_variable AND id_language = :id_language", array(":id_variable" => $data->id, ":id_language" => '.$id.')))?'.$view.':'.$add.'',
        'htmlOptions' => array('style' => 'width: 30px;'),
    );
    $columns[] = $item;
}
$columns[] = "comment";
$columns[] = $buttons;

$this->widget('zii.widgets.grid.CGridView', array(
    'enablePagination' => true,
    'dataProvider'=> $resources->search(),
    'columns'=>$columns,
));
?>

<div class="_button_row">
    <div class="_button" onclick="location='<?= $this->adminPath ?>/resourcevar'"><?= Yii::t('resource', 'Add variable') ?></div>
</div>



