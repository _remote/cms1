<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
        Yii::t('main', 'Site menu') => Yii::app()->params['adminPath']."/menus",
        Yii::t('menu', "Sort menu items"),
);
?>
<h1><?= Yii::t('menu', "Sort menu items for '{language}' language", array("{language}" => $lang->name)) ?></h1>
<script type="text/javascript">
    $(function() {
        setSortable(".sortable");
    });
    
    function setSortable(selector) {
        $(selector).sortable({
            "change": function( event, ui ) {
                $("#save_position").animate({opacity: 1}, 500);
            },
        });
        $(selector).disableSelection();
    }
    
    function saveAllPosition() {
        $("#sortableField > ul").each(function(index){
            var id = parseInt($(this).attr("rel"));
            console.log(id);
            savePosition(this);
        });
    }
    
    function savePosition(selector) {
        var i = 1;
        var data = new Array();
        $(selector).find("li").each(function(index){
            var id = parseInt($(this).attr("rel"));
            data[id] = i++;
        });
        $.post("", {data:data, ajax:1}, function(receive) {
            if (receive.result == true) {
                $("#save_position").animate({opacity: 0}, 500);
            }
            else alert("Runtime Error action. Try again later.");
        }, "json");
    }
    
    function loadSubItems(el) {
        var id = parseInt($(el).attr("rel"));
        var lang = $(el).attr("lang");
        $(el).parent("ul").nextAll(".sortable").remove();
        $.post("<?= $this->adminPath ?>/loadSubMenu", {id:id, lang:lang}, function(data) {
            if (data) {
                $(el).parent("ul").after(data);
                setSortable("#sub_" + id);
            }
        }, "html");
    }
</script>
<div id="sortableField">
<?php 
$this->widget('zii.widgets.CMenu',array(
    "id" => "main_menu",
    'items'=> Menu::model()->loadItems($lang->type, true),
    'htmlOptions' => array('class' => 'sortable', "rel" => 0),
));   
?>
</div>

<div class="_button_row">
    <div class="_button" id="save_position" onclick="saveAllPosition(); return false;"><?= Yii::t('menu', 'Save position') ?></div>
</div>