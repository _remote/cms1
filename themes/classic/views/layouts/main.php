<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
        <meta name="description" content="<?= $this->getResourcesValue("description"); ?>" />
        <meta name="keywords" content="<?= $this->_keywords;?>" />
        <meta name="copyright" content="&copy; <?php echo date('Y'); ?> Vajra Garuda" />
        <meta name="robots" content="index, follow" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
        <?php $time = time(); ?>
        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/main.js?t='.$time); ?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
    <!-- VK Widget -->
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
    <script type="text/javascript">
        VK.Widgets.Group("vk_groups", {mode: 0, width: "200", height: "250", color1: 'FFFFFF', color2: '2B587A', color3: '0B7CA9'}, <?= Yii::app()->params['contacts']['vk_gp_id'] ?>);
    </script>
    <!-- VK Widget -->
    <!--<script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>-->
    <div id="wrap"> 
        <div class="content">
            <div class="header">
                <div class="logo">
                    <a href="/">
                        <img src="/images/emblem<?= (Yii::app()->language != "ru")?"_en":"" ?>.jpg">
                    </a>
                </div>
                <div class="montain">
                        
<!--                        <form class="form-search">
                            <input type="text" class="span2 search-query">
                            <button type="submit" class="btn">Найти</button>
                        </form>-->
                </div>
            </div>
            <div class="body">
                <div class="left_nav">
                    <div class="menu">
                        <?php 
                        $this->widget('ext.liMenuVert.LiMenuWidget',array(
                            'name' => "main_menu",
                            'items'=> Menu::model()->loadFullMenu(Yii::app()->language),
                            //'htmlOptions' => array("rel" => 0),
                        ));
                        ?>
                        
                    </div>
                    <div class="form-search">
                        <form action="/search" method="post">
                            <input type="text" class="search-query" value="<?= $this->_search; ?>" id="search" name="search" placeholder="<?= $this->getResourcesValue("search"); ?>"/>
                        </form>
                    </div>
                   
                    <div class="contacts">
                        <div class="_header">Контакты</div>
                        <ul> 
                            <li><img src="http://mystatus.skype.com/smallicon/<?= Yii::app()->params['contacts']['skype'] ?>" style="border: none;" width="16" height="16" alt="My status" /><a href="skype:<?= Yii::app()->params['contacts']['skype'] ?>?chat"><?= Yii::app()->params['contacts']['skype'] ?></a></li>
                            <li><img src="/images/email.png"><a href="mailto:<?= Yii::app()->params['contacts']['email'] ?>"><?= Yii::app()->params['contacts']['email'] ?></a></li>
                            <li><img src="/images/telephone.png"><?= Yii::app()->params['contacts']['telephone'] ?></li>
                        </ul>
                    </div>
                    <div class="socials vk" id="vk_groups"></div>
                    <!--
                    <div class="search">
                        <?php
                        //$this->renderPartial("_google_search");
                        ?>
                    </div>
                    <div class="counters1">
                        <?php
                        //$this->renderPartial("_counters");
                        ?>
                    </div>
                    -->
                </div> 

                <div class="body_content">
                    <div class="language">
                        <ul class="items">
                            <?php
                            if ($this->_languages) {
                                foreach ($this->_languages as $lng) {
                                    echo "<li><a href=\"/{$lng['language']['type']}/{$this->_path}\" style=\"background-image: url('{$lng['language']['icon']}');\">{$lng['language']['name']}</a></li>";
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
        <!-- page -->
    </div>
    <div class="footer">
        <?php
        $this->renderPartial("_footer");
        ?>
    </div>
    <!-- footer -->
</body>
</html>