<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
        Yii::t('main', "Pages") => Yii::app()->params['adminPath']."/pages",
);
if ($language)
    $this->breadcrumbs = array_merge($this->breadcrumbs, array("{$language->type}" => Yii::app()->params['adminPath']."/pages/".$language->id));
array_push($this->breadcrumbs, (Yii::t('page', 'n#Add new page|!n#{page}', array($model->isNewRecord, "{page}" => $model->title))));
?>
<h1><?= Yii::t('page', 'n#Add new page|!n#View page', array($model->isNewRecord)) ?></h1>

<script type="text/javascript">
    function chooseArticle() {
        var id = $("#Page_id_article").val();
        $("#Image_id_article").val(id);
        $("ul.image_list").empty();
        $.post("<?= Yii::app()->params['adminPath'] ?>/loadArticleImages", {id:id}, function(data) {
            if (data) $("ul.image_list").html(data);
        }, "html");
    }
    
    $(function(){
        $( "#dialog" ).dialog({
            autoOpen: false,
            modal: true,
            close: function( event, ui ) {
                $(this).dialog( "destroy" );
            }
        });
        chooseArticle();
    });
</script>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-page-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>true,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note"><?= Yii::t('main', 'Fields with') ?> <span class="required">*</span> <?= Yii::t('main', 'are required') ?>.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php
        if ($language) 
            echo Chtml::hiddenField('lid', $language->id);
        ?>

	<div class="row">
                <?php
                $models = Language::model()->findAll(array('order' => 'id'));
                $list = CHtml::listData($models, 'id', 'name');
                ?>
		<?php echo $form->labelEx($model,'id_language'); ?>
		<?php echo $form->dropDownList($model, 'id_language', $list, array("style" => "width: 200px;")); ?>
		<?php echo $form->error($model,'id_language'); ?>
	</div>

	<div class="row">
                <?php
                $models = Article::model()->findAll(array('order' => 'id'));
                $list = CHtml::listData($models, 'id', 'path');
                ?>
		<?php echo $form->labelEx($model,'id_article'); ?>
		<?php echo $form->dropDownList($model, 'id_article', $list, array("style" => "width: 200px;", "onchange" => "chooseArticle();")); ?>
		<?php echo $form->error($model,'id_article'); ?>
	</div>

	<div class="row">
            <?php echo $form->labelEx($model,'title'); ?>
            <?php echo $form->textField($model,'title', array("style" => "width: 500px;")); ?>
            <?php echo $form->error($model,'title'); ?>
	</div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'keywords'); ?>
            <?php echo $form->textField($model,'keywords', array("style" => "width: 935px;")); ?>
            <?php echo $form->error($model,'keywords'); ?>
	</div>

	<div class="row" style="position: relative;">
            <?php $this->renderPartial("_photoPanel", array("model" => $model)); ?>
            <?php echo $form->labelEx($model,'content'); ?>
            <?php 
            $this->widget('ext.tinymce.TinyMCEWidget', array(
                'model'=>$model,
                'attribute'=>'content',
                'options'=>array(
                   'language'=>'ru',
                   'width'=>'100%',
                   'height'=>'500px',
                ),
            ));
            ?>
            <?php echo $form->error($model,'content'); ?>
	</div>
        
        <?php if (!empty($model->doc_file)) { ?>
        <div class="row">
            <?php echo $form->labelEx($model, Yii::t('page', "Document File")); ?>
            <?php echo Yii::t('main', "Path").": {$model->doc_file} ".CHtml::link(CHtml::encode(Yii::t('main', "Download")), array($model->doc_file)); ?>
	</div>
        <?php } ?>
        
        <div class="row">
            <?php echo $form->labelEx($model,'_file'); ?>
            <?php echo $form->fileField($model,'_file'); ?>
            <?php echo $form->error($model,'_file'); ?>
	</div>
        
        <div class="row">
            <?php echo $form->labelEx($model,'is_show'); ?>
            <?php //echo $form->checkBox($model,'is_show', array("style" => "float: none;")); ?>
            <?php 
            echo $form->dropDownList($model, "is_show",
                array(0 => Yii::t('main', "no"), 1 => Yii::t('main', "yes")),
                array("style" => "width: 70px;")
            );
            ?>
            <?php echo $form->error($model,'is_show'); ?>
	</div>

	<?php if (!$model->isNewRecord) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'create_date'); ?>
		<?php echo $form->textField($model,'create_date', array("disabled" => "disabled")); ?>
		<?php echo $form->error($model,'create_date'); ?>
	</div>
        <?php } ?>

        <?php if ($model->update_date) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'update_date'); ?>
		<?php echo $form->textField($model,'update_date', array("disabled" => "disabled")); ?>
		<?php echo $form->error($model,'update_date'); ?>
	</div>
        <?php } ?>


	<div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('main', 'n#Create|!n#Save', array($model->isNewRecord))); ?>
	</div>

<?php $this->endWidget(); ?>      
</div><!-- form -->

<div id="dialog" title="<?= Yii::t('image', "Upload photo dialog") ?>">
    <?php $this->renderPartial("imageUpload", array("model" => new Image, "page" => $model)); ?>
</div>