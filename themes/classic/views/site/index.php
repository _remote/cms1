<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->pageTitle=Yii::app()->name . ' - '.$this->_title;

?>
<h1><?= $this->_title ?></h1>
<div class="body"> 
    <?= $this->_content ?>
    <?php 
    if (!empty($this->_file)) {
    ?>
    <div class="_doc_link">
        <a href="<?= $this->_file ?>">Скачать полную версию</a>
    </div>
    <?php
    }
    ?>
</div>
