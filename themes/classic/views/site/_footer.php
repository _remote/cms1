<div class="line"></div>
<div class="nav">
    <?php
    $this->widget('zii.widgets.CMenu',array(
        'items'=> Menu::model()->loadItems(Yii::app()->language),
    )); 
    ?>
    &copy; <?php echo date('Y'); ?> <a href="mailto:<?= Yii::app()->params['adminEmail'] ?>"><?= $this->getResourcesValue("garuda"); ?></a>. <?= $this->getResourcesValue("copyright"); ?>.<br/>
</div>
<div class="counters_panel">
    <div class="c1">
        <?php
        $this->renderPartial("_catalogs");
        $this->renderPartial("_counters");
        ?>
    </div>
</div>

