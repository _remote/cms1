<?php 
$items = array();
foreach ($categories as $id => $category) {
    if (!$active) $active = $id;
    $item['label'] = $category;
    $item['url'] = array("/partners?category={$id}");
    if ($active == $id)
        $item['active'] = true;
    else $item['active'] = false;
    $items[] = $item;
}
$this->widget('zii.widgets.CMenu',array(
    'id' => 'category_list',
    'items'=> $items,
    'htmlOptions' => array("style" => "margin-bottom: 15px;"),
));

$html = "<ul id=\"partner_list\">";
$index = 1;
foreach ($partners as $partner) {
    $html .= "<li>";
    $link = $partner->partner_link;
    $desc = $partner->description;
    $html .= ($index++).". ";
    if ($partner->is_icon)
        $html .= $link." ".$desc;
    else
        $html .= $desc." ".$link;
    $html .= "</li>";
}
echo $html .= "</ul>";
?>

<p style="margin-top: 15px; color: red;"> По вопросам обмена ссылками обращайтесь по адресу  <a href="mailto:admin@garuda.com.ua?subject=Обмен ссылками" class="">admin@garuda.com.ua</a></p>

