<script type="text/javascript">
    function photoSubmit() {
        $("#image-image-form").ajaxForm({
            //target: '.preview',
            url: "<?= Yii::app()->params['adminPath'] ?>/image",
            data: {},
            dataType: 'json',
            success: function(data) { 
                $("#AjaxLoader").hide();  
                if(data.status=="success"){
                    $("#dialog").dialog( "destroy" );
                    $(".image_list").append("<li>" + data.image + "</li>");
                }
                else{
                    var _ul = $("#image-image-form_es_").find("ul");
                    $(_ul).empty();
                    $.each(data, function(key, val) {
                        $(_ul).append("<li class=\'item\'>" + val + "</li>");
                        //$("#image-image-form #"+key+"_em_").text(val);                                                    
                        //$("#image-image-form #"+key+"_em_").show();
                    });
                    $("#image-image-form_es_").show();
                }   
            } 
        }).submit();
    }
</script>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'image-image-form',
        'action' => Yii::app()->params['adminPath']."/image",
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    )); ?>

    <p class="note"><?= Yii::t('main', 'Fields with') ?> <span class="required">*</span> <?= Yii::t('main', 'are required') ?>.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->hiddenField($model,'id_article'/*, array("value" => $page->id_article)*/); ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'image'); ?>
        <?php echo $form->fileField($model,'image'); ?>
        <?php echo $form->error($model,'image'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textField($model,'title'); ?>
        <?php echo $form->error($model,'title'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::button(Yii::t('main', 'n#Create|!n#Save', array($model->isNewRecord)), array("onclick" => "photoSubmit();")); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->