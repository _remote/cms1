<script type="text/javascript">
    function insertLink () {
        var href = $("#pages_menu").val();
        var text = $("#pages_menu").text();
        var title = $("#title_for_link").val();
        var anchor = $("#_anchor").val();
        var link = '<a href="/' + href + (anchor?'#' + anchor:"") + '">' + (title?title:text) + '</a>';
        tinyMCE.execCommand('mceInsertContent', false, link);
        $("#_anchor").val("");
    }
    
    function insertImage(img) {
        var src = $(img).attr("src");
        var alt = $(img).attr("alt");
        tinyMCE.execCommand('mceInsertContent', false, "<img src=\"" + src + "\" alt=\"" + alt + "\" title=\"" + alt + "\">");
    }
    
    function uploadPhoto() {
        $( "#dialog" ).dialog({
            autoOpen: false,
            modal: true,
            close: function( event, ui ) {
                $(this).dialog( "destroy" );
            }
        });
        $( "#dialog" ).dialog( "open" );
    }
    
    function loadPages(el) {
        var id = $(el).val()
        $.post("<?= Yii::app()->params['adminPath'] ?>/loadPagesMenu", {id:id, selector:"pages_menu"}, function(data) {
            if (data) {
                $("#pages_for_language").html(data);
                showButton();
            }
        }, "html");
    }
    
    function openPanel(panel) {
        var body = $(panel).find("div.nav-body");
        if (!$(body).hasClass("_opened")) {
            $(body).animate({width: 200}, 200);
            $(body).addClass("_opened");
            $(".nav-images").css("z-index", 1000);
        }
        $(".nav-images").removeAttr("title");
    }
    
    function closePanel(ms) {
        ms = ms || 200;
        var body = $("div.nav-body");
        if (!$(body).hasClass("_opened")) return;
        $(body).animate({width: 1}, ms, function() {
            $(body).removeClass("_opened");
            $(".nav-images").attr("title", "<?= Yii::t('image', 'Photo panel (click to open)') ?>");
            $(".nav-images").css("z-index", 1);
        });
        
    }
    
    $(function(){
        loadPages($("#language"));
        $( "#dialog" ).dialog({
            autoOpen: false,
            modal: true,
            close: function( event, ui ) {
                $(this).dialog( "destroy" );
            }
        });
        setTimeout('closePanel(500)', 1000)
    });
</script>

<div class="nav-images" onclick="openPanel(this);">
    <div class="nav-body _opened">
        <div class="close" onclick="closePanel();">X</div>
        <div class="title"><?= Yii::t('image', 'Images list') ?></div>
        <div class="_content">
            <ul class="image_list">
                <?php
                $images = Image::model()->findAll("id_article = :id_article", array(":id_article" => $model->id_article));
                if ($images) {
                    foreach($images as $item) {
                        $title = ($item->title)?"alt=\"{$item->title}\" title=\"{$item->title}\"":"";
                        echo "<li><img src=\"{$item['path']}\" {$title} onclick=\"insertImage(this);\"></li>";
                    }
                }
                ?>
            </ul>
            <div class="_add" onclick="uploadPhoto();"><?= Yii::t('image', 'Upload photo') ?></div>
        </div>
        <div class="title2"><?= Yii::t('image', 'Insert link'); ?></div>
        <div class="_content2">
            <label><?= Yii::t('image', 'Choose language') ?></label>
            <?php
            $models = Language::model()->findAll(array('order' => 'id'));
            $list = CHtml::listData($models, 'id', 'name');
            echo CHtml::dropDownList("language", null, $list, array("onChange" => "loadPages(this); return false;"));
            ?>
            <label><?= Yii::t('image', 'Choose page') ?></label>
            <div id="pages_for_language"></div>
            <div id="pages_anchor">#
                <input type="text" id="_anchor" value="" title="Anchor">
            </div>
            <label><?= Yii::t('image', 'Title of link') ?></label>
            <input type="text" id="title_for_link" value="">
            <div class="_add" onclick="insertLink();"><?= Yii::t('image', 'Add link') ?></div>
        </div>
    </div>
</div>

