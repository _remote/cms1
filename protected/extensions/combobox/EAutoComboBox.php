<?php
class EAutoComboBox extends CInputWidget {

    public $data;
    public $options;

    public function run() {
        list($name, $id) = $this->resolveNameID();
        
        $this->options['name'] = $name;

        // Publishing assets.
        $assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__));

        // Registering javascript.
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerCoreScript('jquery.ui' );
        $cs->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');
        
        $time = time();
        $cs->registerScriptFile($assets . '/assets/combobox.js');
        $cs->registerCssFile($assets . '/assets/combobox.css');
        
        $cs->registerScript(
            'Yii.' . get_class($this) . '#' . $id, '$(function(){$("#'.$name.'").combobox();});'
        );
        $html = CHtml::dropDownList($name, null, $this->data, $this->htmlOptions);

        echo $html;
    }
};
?>