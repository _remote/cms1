<?php
$this->breadcrumbs=array(Yii::t('main', 'Admin') => Yii::app()->params['adminPath'],
    Yii::t('image', 'View videos'),
);
?>

<h1><?= Yii::t('video', 'View site videos') ?></h1>
<?php
$columns = array(
    'id' => array(
        'name' => 'id',
        'htmlOptions' => array('style' => 'width: 30px; text-align: center;'),
    ),
    'title',
    'html_code' => [
        'name'=>'html_code',
        'type'=>'html',
        'htmlOptions' => ['align' => 'center'],
        'value'=> function ($data) {
            if (preg_match("/src=\"(https?:\/\/)?([\w\.]+)\.([a-z]{2,6}\.?)(\/[\w\.-]*)*\/?\"/", $data->html_code, $src)) {
                $url = "https://www.youtube.com/embed".$src[4];
                return $url;
            }
            return $data->html_code;
        },
    ],
    'create_date',
    array(
        'class'=>'CButtonColumn',
        'template'=>'{update} {view} {delete}',
        'buttons'=>array
        (
            'view' => array (
                'url'=> function ($data) {
                    if (preg_match("/src=\"(https?:\/\/)?([\w\.]+)\.([a-z]{2,6}\.?)(\/[\w\.-]*)*\/?\"/", $data->html_code, $src)) {
                        $url = "https://www.youtube.com/embed".$src[4];
                        return $url;
                    }
                },
                'options'=>array("target"=>"_blank"),
            ),
            'update' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/".strtolower(get_class($data))."/$data->id")',
            ),
            'delete' => array (
                'url'=>'Yii::app()->createUrl("/'.$this->adminPath.'/delete/".strtolower(get_class($data))."/$data->id")',
            ),
        ),
    ),
);
$this->widget('zii.widgets.grid.CGridView', array(
    'enablePagination' => true,
    'dataProvider'=> new CActiveDataProvider($model),
    'columns'=>$columns,
));
?>

<div class="_button_row">
    <div class="_button" onclick="location='<?= $this->adminPath ?>/video'"><?= Yii::t('video', 'Add video') ?></div>
</div>

