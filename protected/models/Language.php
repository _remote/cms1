<?php

/**
 * This is the model class for table "{{language}}".
 *
 * The followings are the available columns in table '{{language}}':
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $icon
 * @property integer $is_default
 */
class Language extends CActiveRecord {
    
    public $image;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{language}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('is_default', 'numerical', 'integerOnly'=>true),
                array('name', 'length', 'max'=>50),
                array('type', 'length', 'max'=>3),
                //array('icon', 'length', 'max'=>1500),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, name, type, icon, is_default', 'safe', 'on'=>'search'),
                array('image', 'file', 'types'=>'jpg, gif, png', 'message' => Yii::t('language', 'Please choose icon file.'), 'allowEmpty'=>true),
            );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
                    'id' => Yii::t('language', 'ID'),
                    'name' => Yii::t('language', 'Name'),
                    'type' => Yii::t('language', 'Type'),
                    'icon' => Yii::t('language', 'Icon'),
                    'is_default' => Yii::t('language', 'Is Default'),
                    'image' => Yii::t('language', 'Choose icon'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('is_default',$this->is_default);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        protected function beforeDelete(){
            if(!parent::beforeDelete())
                return false;
            $this->deleteDocument(); // удалили модель? удаляем и файл
            return true;
        }

        public function deleteDocument(){
            $documentPath=Yii::getPathOfAlias('webroot.upload').$this->icon;
            if(is_file($documentPath))
                unlink($documentPath);
        }
        
//        public function countPages() {
//            return Page
//        }

        /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Language the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
