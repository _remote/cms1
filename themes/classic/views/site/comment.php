
<h1><?= $this->getResourcesValue("comments_header") ?></h1>

<ul class="comment_list">
<?php
if ($comments) {
    foreach ($comments as $comment) {
        $name = ($comment->name)?$comment->name:"Anonym";
        echo "<li><span>{$name}:</span> {$comment->text}</li>";
    }
}
else echo $this->getResourcesValue("no_comments");
?>
</ul>

<h1 id="add"><?= $this->getResourcesValue("add_comment") ?></h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comment-comment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>true,
        'action' => '/comment#add',
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p>-->

	<?php echo $form->errorSummary($model); ?>
        <?php 
        if (++$is_approved)
            echo "<div class=\"_approved\">".$this->getResourcesValue("approved_comment")."</div>"; 
        ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name', array("style" => "margin-left: 64px;")); ?>
		<?php //echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text', array("style" => "width: 600px; height: 100px; margin-left: 5px; margin-top: 7px; display: inline-block;")); ?>
		<?php //echo $form->error($model,'text'); ?>
	</div>

<!--	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php //echo $form->error($model,'email'); ?>
	</div>-->

        <div class="row">
        <?php echo CHtml::activeLabel($model, 'verifyCode'); ?>
        <?php $this->widget('application.extensions.recaptcha.EReCaptcha', 
           array('model'=> $model, 'attribute'=>'verifyCode',
                 'theme'=>'blue', 'language'=> Yii::app()->language, 
                 'publicKey'=>'6LejOP4SAAAAAA17p6J7_UGymGBM6XVuE0PXYXQQ')) 
        ?>
        </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Отправить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->