<?php
$html = $data['html_code'];
if (preg_match("/src=\"(https?:\/\/)?([\w\.]+)\.([a-z]{2,6}\.?)(\/[\w\.-]*)*\/?\"/", $data['html_code'], $src)) {
    $url = "https://www.youtube.com/embed".$src[4];
}
else $url = false;
?>
<div class="video_frame">
    <?php
    if ($url) {
        echo '<iframe width="360" height="270" src="'.$url.'" frameborder="0" allowfullscreen></iframe>';
    } else {
        echo htmlspecialchars_decode($data['html_code']); 
    }
    ?>
</div>

