<?php
return array(
    "n#Add new comment|!n#View comment from {name}" => "n#Новый отзыв|!n#Просмотр отзыва от {name}",
    "n#Add new comment|!n#View comment" => "n#Добавление нового отзыва|!n#Просмотр отзыва",
    "View users comments" => "Отзывы посетителей",
    "Table of users comments" => "Таблица отзывов посетителей",
    "Add comment" => "Добавить отзыв",
    "Id Language" => "Id языка",
    "Name" => "Имя",
    "Email" => "e-Mail",
    "Comment" => "Сообщение",
    "Create Date" => "Создан",
    "Is Approved" => "Утвержден",
    "Language" => "Язык",
    "Verify Code" => "Код проверки",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
    "" => "",
);

