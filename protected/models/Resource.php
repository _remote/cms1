<?php

/**
 * This is the model class for table "{{resource}}".
 *
 * The followings are the available columns in table '{{resource}}':
 * @property integer $id
 * @property integer $id_language
 * @property integer $id_variable
 * @property string $value
 */
class Resource extends CActiveRecord {
    
    public $variable_name;
    public $language_search;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{resource}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_language, id_variable', 'numerical', 'integerOnly'=>true),
			array('value', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_language, id_variable, value, variable_name, language_search', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'variable' => array(self::BELONGS_TO, 'Resourcevar', 'id_variable'),
                    'language' => array(self::BELONGS_TO, 'Language', 'id_language'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
            return array(
                'id' => Yii::t('resource', 'ID'),
                'id_language' => Yii::t('resource', 'Id Language'),
                'id_variable' => Yii::t('resource', 'Id Variable'),
                'value' => Yii::t('resource', 'Value'),
                //'variable_name' => Yii::t('resource', 'Name'),
                'language_search' => Yii::t('resource', 'Language'),
            );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
                $criteria->with = array("language", "variable");
		$criteria->compare('id',$this->id);
		$criteria->compare('id_language',$this->id_language);
		$criteria->compare('id_variable',$this->id_variable);
		$criteria->compare('value',$this->value,true);
                
                $criteria->compare( 'variable.name', $this->variable_name, true );
                $criteria->compare( 'language.name', $this->language_search, true );
                
                return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'sort'=>array(
                        'attributes'=>array(
                            'variable_name'=>array(
                                'asc'=>'variable.name',
                                'desc'=>'variable.name DESC',
                            ),
                            'language_search'=>array(
                                'asc'=>'language.name',
                                'desc'=>'language.name DESC',
                            ),
                            '*',
                        ),
                    ),
                    'pagination' => array(
                        'pageSize' => Yii::app()->params['pageSize'],
                    ),
                ));
	}
        
        public function getResourcesList($language = "ru") {
            $criteria=new CDbCriteria;
            $criteria->with = array("language" , "variable");
            $criteria->condition = "language.`type` = :language";
            $criteria->params = array(":language" => $language);
            $criteria->order = "variable.`name` ASC";
            $items = $this->findAll($criteria);
            return $list = CHtml::listData($items, 'variable.name', 'value');
        }
        
        public function getResource($name = "", $language = "ru") {
            $criteria=new CDbCriteria;
            $criteria->with = array("language" , "variable");
            $criteria->condition = "variable.`name` = :name AND language.type = :language";
            $criteria->params = array(":name" => $name, ":language" => $language);
            $criteria->order = "variable.`name` ASC";
            $res = $this->find($criteria);
            return ($res)?$res->value:"";
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Resources the static model class
	 */
	public static function model($className=__CLASS__) {
            return parent::model($className);
	}
}
